/*----------------------------------------------------------------------------*/
#include "my1xmesh.h"
#include "my1comlib.h"
#include "my1cons.h"
#include "my1list.h"
#include "my1dbase.h"
#include "my1wsock.h"
#include "mongoose.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include <signal.h>
#include <sys/time.h>
/*----------------------------------------------------------------------------*/
#ifndef MY1APP_PROGNAME
#define MY1APP_PROGNAME "my1wsnbase"
#endif
#ifndef MY1APP_PROGVERS
#define MY1APP_PROGVERS "build"
#endif
#ifndef MY1APP_PROGINFO
#define MY1APP_PROGINFO "Wireless Sensor Network Base/Server"
#endif
/*----------------------------------------------------------------------------*/
#define ERROR_GENERAL -1
#define ERROR_NO_PORT -2
#define ERROR_NO_BASE -3
/*----------------------------------------------------------------------------*/
#define STR_BUFF_SIZE 80
#define FILENAME_SIZE 80
#define DEFAULT_DB_FILENAME "basenode"
#define DEFAULT_DB_FILETEXT "default.txt"
#define DEFAULT_DB_FILEPATH "sens/"
#define MESSAGE_START "Initialization done. Starting main loop...\n\n"
/*----------------------------------------------------------------------------*/
#define MAX_WEBSOCK_COUNT 23
/*----------------------------------------------------------------------------*/
typedef struct _wsconn
{
	struct mg_connection *pconn;
	struct mg_request_info *pinfo;
	struct tm atime;
}
wsconn;
/*----------------------------------------------------------------------------*/
static my1list conns;
static my1list nodes;
static my1list sensb;
/*----------------------------------------------------------------------------*/
#define TIMER_TIMEOUT_S 1
#define MINUTE_EXPIRE_S 60
#define DEFAULT_TIMEOUT 10
#define DEFAULT_TIMECHK 3
#define DUMMY_TIMEOUT_S 2
#define CLIENT_TIME_OUT 1
#define DUP_DATA_TIME_S 10
/*----------------------------------------------------------------------------*/
static int flag_verbose = 0, flag_display = 0;
static int timer_minute = MINUTE_EXPIRE_S;
static int timer_expire;
static int dummy_expire;
static int timeout = DEFAULT_TIMEOUT;
static int timechk = DEFAULT_TIMECHK;
static FILE *plogfile = 0x0;
/*----------------------------------------------------------------------------*/
void timer_handler(int signum)
{
	(void) signum;
	/* second-base timers */
	if(dummy_expire>0) dummy_expire--;
	/* check minute mark */
	if(timer_minute>0) timer_minute--;
	if(timer_minute==0)
	{
		timer_minute = MINUTE_EXPIRE_S;
		/* minute-base timers */
		if(timer_expire>0) timer_expire--;
	}
}
/*----------------------------------------------------------------------------*/
typedef void (*printf_chk)(char* fmt, ...);
/*----------------------------------------------------------------------------*/
void printf_log(char* fmt, ...)
{
	va_list args;
	/** check for valid log file */
	if(plogfile)
	{
		va_start(args,fmt);
		vfprintf(plogfile,fmt,args);
		va_end(args);
	}
	/** check for verbose mode */
	if(flag_verbose||flag_display)
	{
		va_start(args,fmt);
		vfprintf(stdout,fmt,args);
		va_end(args);
	}
}
/*----------------------------------------------------------------------------*/
void printf_err(char* fmt, ...)
{
	va_list args;
	va_start(args,fmt);
	vfprintf(stderr,fmt,args);
	va_end(args);
	/** check for valid log file */
	if(plogfile)
	{
		va_start(args,fmt);
		vfprintf(plogfile,fmt,args);
		va_end(args);
	}
}
/*----------------------------------------------------------------------------*/
void printf_req(char* fmt, ...)
{
	va_list args;
	/** print only in verbose mode */
	if(flag_verbose||flag_display)
	{
		va_start(args,fmt);
		vfprintf(stdout,fmt,args);
		va_end(args);
	}
}
/*----------------------------------------------------------------------------*/
void printf_time(printf_chk go_print)
{
	char timestring[DATETIME_FORMAT_LEN];
	time_t currtime = time(0x0);
	struct tm timestamp = *localtime(&currtime);
	sprintf(timestring,DATETIME_FORMAT,
		(1900+timestamp.tm_year),(timestamp.tm_mon+1),timestamp.tm_mday,
		timestamp.tm_hour,timestamp.tm_min,timestamp.tm_sec);
	go_print("[[%s]] ",timestring); 
}
/*----------------------------------------------------------------------------*/
void about(void)
{
	printf("Command-line use:\n");
	printf("  %s [options]\n\n",MY1APP_PROGNAME);
	printf("Options are:\n");
	printf("  --port <number>      : port number between 1-%d.\n",MAX_COM_PORT);
	printf("  --tty <device>       : alternate serial device name.\n");
	printf("  --dbname <dbname>    : alternate DB file name.\n");
	printf("  --dbtext <dbtext>    : alternate board DB text file name.\n");
	printf("  --baud <baudrate>    : alternate baudrate.\n");
	printf("  --amgroup <AM group> : filter AM group.\n");
	printf("  --appid <App ID>     : filter application ID.\n");
	printf("  --timeout <minutes>  : node timeout removal.\n");
	printf("  --timechk <minutes>  : timeout checking interval.\n");
	printf("  --log     : create a log of events.\n");
	printf("  --noserve : do not start web server.\n");
	printf("  --dummy   : run without serial interface.\n");
	printf("  --beat    : show heartbeat (default: ignored).\n");
	printf("  --verbose : show useful event messages.\n");
	printf("  --scan    : scan and list ports. overrides above options.\n");
	printf("  --help    : show this message. overrides ALL above options.\n\n");
}
/*----------------------------------------------------------------------------*/
void print_portscan(ASerialPort_t* aPort)
{
	int test, cCount = 0;
	printf("\n--------------------\n");
	printf("COM Port Scan Result\n");
	printf("--------------------\n");
	for(test=1;test<=MAX_COM_PORT;test++)
	{
		if(check_serial(aPort,test))
		{
			printf("%s%d: ",aPort->mPortName,COM_PORT(test));
			cCount++;
			printf("Ready.\n");
		}
	}
	printf("\nDetected Port(s): %d (%s)\n\n",cCount,aPort->mPortName);
}
/*----------------------------------------------------------------------------*/
void print_frame_wsn(wsnode* pnode, printf_chk go_print)
{
	int loop;
	if(pnode->ptiny->am_type==AM_DATA2BASE)
	{
		/** tinyos header */
		go_print("[TINYOS] => Group: %02X, Length: %02X",
			pnode->ptiny->am_group,pnode->ptiny->length);
		/** xmesh header */
		if(pnode->countp>XMESH_SENS_OFFSET)
		{
			go_print("\n\t[XMESH] => Origin: %04X, Source: %04X, ",
				pnode->pmesh->org_addr,pnode->pmesh->src_addr);
			go_print("Sequence: %04X, AppID: %02X",
				pnode->pmesh->seq_num, pnode->pmesh->app_id);
		}
		/** xsense header */
		if(pnode->countp>XMESH_DATA_OFFSET)
		{
			go_print("\n\t[BOARD] => BoardID: %02X, PacketID: %02X",
				pnode->psens->board_id,pnode->psens->packet_id);
			go_print("\n\t[DATA] => ");
			for(loop=0;loop<pnode->dcount;loop++)
				go_print("[%04X] ",pnode->pdata[loop]);
			go_print("{%d/%d}",loop,pnode->dcount);
		}
		go_print("\n");
	}
	else if(pnode->ptiny->am_type==AM_HEALTH)
	{
		if(pnode->ptiny->length>XMESH_HLTHHEAD_SIZE)
		{
			xmesh_health_header *phealth = (xmesh_health_header*)pnode->psens;
			int hood_count = (int)(phealth->type_node&0x0F);
			 /** statistic */
			if(phealth->type==0x01&&
				pnode->ptiny->length>=XMESH_STATPACK_SIZE)
			{
				xmesh_health_stat *pstat = (xmesh_health_stat*)phealth;
				/** tinyos header */
				go_print("[TINYOS] => Group: %02X, Length: %02X",
					pnode->ptiny->am_group,pnode->ptiny->length);
				go_print("\n\t[XMESH] => Origin: %04X, Source: %04X, ",
					pnode->pmesh->org_addr,pnode->pmesh->src_addr);
				go_print("Sequence: %04X, AppID: %02X",
					pnode->pmesh->seq_num, pnode->pmesh->app_id);
				go_print("\n\t[HEALTH] => Version: %02X, ",
					phealth->version);
				go_print("Type node: %02X, Type: %02X",
					phealth->type_node, phealth->type);
				go_print("\n\t[STAT] => Sequence: %04X, Battery: %02X",
					pstat->seq_num,pstat->battery_voltage);
				go_print("\n\t[PACK] => #Local: %04X, #FWD: %04X, #DRP: %04X",
					pstat->num_node_pkts,pstat->num_fwd_pkts,
					pstat->num_drop_pkts);
				for(loop=0;loop<hood_count&&
					loop<HEALTH_NODE_MAX_NODE;loop++)
				{
					go_print("\n\t[%04X] => Parent: %04X, Hop: %02X, ",
						pstat->linkinfo[loop].node_id,
						pstat->linkinfo[loop].hop_count);
					go_print("Link Q: %02X, Link I: %02X",
						pstat->linkinfo[loop].link_quality,
						pstat->linkinfo[loop].radio_link_indicator);
				}
				go_print("\n");
			}
			/** neighborhood */
			else if(phealth->type==0x02&&
				pnode->ptiny->length>=XMESH_HOODPACK_SIZE)
			{
				xmesh_health_hood *phood = (xmesh_health_hood*)phealth;
				/** tinyos header */
				go_print("[TINYOS] => Group: %02X, Length: %02X",
					pnode->ptiny->am_group,pnode->ptiny->length);
				go_print("\n\t[XMESH] => Origin: %04X, Source: %04X, ",
					pnode->pmesh->org_addr,pnode->pmesh->src_addr);
				go_print("Sequence: %04X, AppID: %02X",
					pnode->pmesh->seq_num, pnode->pmesh->app_id);
				go_print("\n\t[HEALTH] => Version: %02X, ",
					phealth->version);
				go_print("Type node: %02X, Type: %02X",
					phealth->type_node, phealth->type);
				go_print("\n\t[HOOD] => Count: %02X (%d)",
					phood->header.type_node&0x0F,hood_count);
				for(loop=0;loop<hood_count&&
					loop<HEALTH_NODE_MAX_NEIGHBOR;loop++)
				{
					go_print("\n\t[%04X] => Parent: %04X, Hop: %02X, ",
						phood->linkinfo[loop].node_id,
						phood->linkinfo[loop].hop_count);
					go_print("Link Q: %02X, Link I: %02X",
						phood->linkinfo[loop].link_quality,
						phood->linkinfo[loop].radio_link_indicator);
				}
				go_print("\n");
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
int get_total_days(int month, int year)
{
	int totaldays[] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
	int days = totaldays[month], is_leap = 0;
	if(year%400==0) is_leap = 1;
	else if(year%100==0) is_leap = 0;
	else if(year%4==0) is_leap = 1;
	if(is_leap&&month==1)
		days++;
	return days;
}
/*----------------------------------------------------------------------------*/
int get_month_week(struct tm* ptime)
{
	int test, temp, init, last;
	int year = ptime->tm_year+1900;
	int cmon = ptime->tm_mon;
	int cday = ptime->tm_mday;
	int days = get_total_days(cmon, year);
	int wday = ptime->tm_wday;
	/** get first monday & last sunday */
	test = (cday%7);
	if(!test) test = 7;
	temp = (wday-1);
	if(temp<0) temp += 7;
	if(test<=temp) test += 7;
	init = test - temp;
	last = init + 20; /* 3 weeks later, -1 for sunday */
	if(days-last>7) last += 7;
	/* week count */
	if(cday<init) test = 0;
	else test = ((cday-init)/7)+1;
	/** check if need to compress first week */
	if(init<5)
	{
		if(test>0) test--;
	}
	/** check if need to compress final week */
	if((days-last)<4)
	{
		if(cday>last) test--;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int create_dbfilename(char* pfilename, int wtime)
{
	time_t currtime = time(0x0);
	struct tm thistime = *localtime(&currtime);
	int week = get_month_week(&thistime);
	/* create string version for time */
	sprintf(pfilename,"%s-%04d%02dW%d",pfilename,
		(1900+thistime.tm_year),(thistime.tm_mon+1),week);
	/* create name with specific time if requested */
	if(wtime)
	{
		sprintf(pfilename,"%s_%02d%02d%02d%02d",pfilename,thistime.tm_mday,
			thistime.tm_hour,thistime.tm_min,thistime.tm_sec);
	}
	/* append extension */
	sprintf(pfilename,"%s.sqlite",pfilename);
	return week;
}
/*----------------------------------------------------------------------------*/
void check_sensorboard(wsnode* pnode)
{
	if(pnode->data.count) return;
	sboard* pboard = check_sens_list(&sensb,pnode->sensid);
	if(pboard&&(pboard->datacount==pnode->dcount))
	{
		pboard->sensdata.curr = 0x0;
		while(list_iterate(&pboard->sensdata))
		{
			wsdata *pdata = (wsdata*) malloc(sizeof(wsdata));
			pdata->psensdata = (sbdata*) pboard->sensdata.curr->item;
			list_push_item(&pnode->data,(void*)pdata);
		}
	}
}
/*----------------------------------------------------------------------------*/
wsnode* find_node(wsnode* pnode)
{
	list_lock(&nodes);
	wsnode *ptest = 0x0;
	nodes.curr = 0x0;
	while(list_iterate(&nodes))
	{
		wsnode *ptemp = (wsnode*) nodes.curr->item;
		if(ptemp->nodeid==(int)pnode->pmesh->org_addr&&
			ptemp->sensid==(int)pnode->psens->board_id&&
			ptemp->appsid==(int)pnode->pmesh->app_id)
		{
			ptest = ptemp;
			break;
		}
	}
	list_unlock(&nodes);
	return ptest;
}
/*----------------------------------------------------------------------------*/
wsnode* copy_node(wsnode* pnode, wsnode* pinit)
{
	list_lock(&nodes);
	if(!pnode)
	{
		pnode = (wsnode*) malloc(sizeof(wsnode));
		clone_wsnode(pnode,pinit);
		list_push_item(&nodes,(void*)pnode);
		check_sensorboard(pnode);
	}
	/** copy latest frame */
	txdat_wsnode(pnode,pinit);
	list_unlock(&nodes);
	return pnode;
}
/*----------------------------------------------------------------------------*/
void free_node(void* pnode)
{
	clean_wsnode((wsnode*)pnode);
	free(pnode);
}
/*----------------------------------------------------------------------------*/
int free_nodes(void)
{
	list_lock(&nodes);
	list_clean(&nodes,&free_node);
	list_unlock(&nodes);
	return nodes.count;
}
/*----------------------------------------------------------------------------*/
void free_conn(void* pconn)
{
	/** how to check if still valid? how to remove gracefully? */
	/** maybe send websocket close? */
	/** unsigned char buff[2] = { 0x88, 0x00 };
	mg_write((struct mg_connection*)pconn, buff, 2); */
	free(pconn);
}
/*----------------------------------------------------------------------------*/
int free_conns(void)
{
	list_lock(&conns);
	list_clean(&conns,&free_conn);
	list_unlock(&conns);
	return conns.count;
}
/*----------------------------------------------------------------------------*/
my1item* find_conn(struct mg_connection *conn)
{
	conns.curr = 0x0;
	while(list_iterate(&conns))
	{
		wsconn *pconn = (wsconn*) conns.curr->item;
		if(pconn->pconn==conn) break;
	}
	return conns.curr;
}
/*----------------------------------------------------------------------------*/
my1item* check_in_conns(struct mg_connection *conn)
{
	list_lock(&conns);
	my1item *pitem = find_conn(conn);
	if(!pitem)
	{
		wsconn *pconn = (wsconn*) malloc(sizeof(wsconn));
		pconn->pconn = conn;
		pconn->pinfo = mg_get_request_info(conn);
		list_push_item(&conns,(void*)pconn);
	}
	list_unlock(&conns);
	return pitem;
}
/*----------------------------------------------------------------------------*/
my1item* check_out_conns(struct mg_connection *conn)
{
	list_lock(&conns);
	my1item *pitem = find_conn(conn);
	if(pitem)
	{
		wsconn *pconn = (wsconn*) pitem->item;
		list_delete(&conns,pitem,0x0);
		free((void*)pconn);
	}
	list_unlock(&conns);
	return pitem; /* freed! just indicator, should not use this! */
}
/*----------------------------------------------------------------------------*/
char* create_node_json(my1list* nodes, char **pbuffer, int *pcount, char* pinit)
{
	wsnode *pnode;
	my1item *pitem = nodes->init;
	char buffer[128], *pjson = 0x0, *pwebs = 0x0;
	int count = 0, index = 0;
	sprintf(buffer,"{\"type\":\"nodeinfo\",");
	append_string_data(&pjson,&count,buffer);
	/* add in extra info if available */
	if(pinit)
	{
		sprintf(buffer,"\"init\":%s,",pinit);
		append_string_data(&pjson,&count,buffer);
	}
	while(pitem)
	{
		pnode = (wsnode*) pitem->item;
		sprintf(buffer,"\"node%d\":{\"uniqid\":\"%d\",\"nodeid\":\"%d\","
			"\"sensid\":\"%d\",\"appsid\":\"%d\",\"dcount\":\"%d\"},",
			index, pnode->uniqid,pnode->nodeid,pnode->sensid,pnode->appsid,
			pnode->dcount);
		append_string_data(&pjson,&count,buffer);
		index++;
		pitem = pitem->next;
	}
	sprintf(buffer,"\"countN\":\"%d\"}",index);
	append_string_data(&pjson,&count,buffer);
	/** create websocket data */
	pwebs = create_websock_data(pbuffer,pcount,pjson);
	/** free temp heap */
	if(count>0) free(pjson);
	return pwebs;
}
/*----------------------------------------------------------------------------*/
char* create_data_json(wsnode* pnode, char **pbuffer, int *pcount)
{
	my1item* pitem = 0x0;
	char buffer[128], *pjson = 0x0, *pwebs = 0x0;
	int count = 0, index = 0;
	/** check if there's a valid board */
	if(pnode->dcount==pnode->data.count)
		pitem = pnode->data.init;
	sprintf(buffer,"{\"type\":\"rawdata\",\"uniqid\":\"%d\","
		"\"timestamp\":\"%s\",\"voltdata\":\"%d\",",
		pnode->uniqid,pnode->timestring,pitem?1:0);
	append_string_data(&pjson,&count,buffer);
	sprintf(buffer,"\"tiny\":{\"dst_addr\":\"%d\",\"am_type\":\"%d\","
		"\"am_group\":\"%d\",\"length\":\"%d\"},",
		pnode->ptiny->dst_addr, pnode->ptiny->am_type,
		pnode->ptiny->am_group,pnode->ptiny->length);
	append_string_data(&pjson,&count,buffer);
	sprintf(buffer,"\"mesh\":{\"source\":\"%d\",\"origin\":\"%d\","
		"\"sequence\":\"%d\",\"app_id\":\"%d\"},",
		pnode->pmesh->src_addr, pnode->pmesh->org_addr,
		pnode->pmesh->seq_num,pnode->pmesh->app_id);
	append_string_data(&pjson,&count,buffer);
	sprintf(buffer,"\"sens\":{\"board_id\":\"%d\","
		"\"pack_id\":\"%d\",\"parent\":\"%d\"},",
		pnode->psens->board_id, pnode->psens->packet_id,
		pnode->psens->parent);
	append_string_data(&pjson,&count,buffer);
	while(index<pnode->dcount)
	{
		if(pitem)
		{
			wsdata* pdata = (wsdata*) pitem->item;
			sprintf(buffer,"\"data%d\":{\"raw\":\"%d\", ",
				index,pdata->rawvalue);
			append_string_data(&pjson,&count,buffer);
			if(pdata->psensdata)
			{
				sprintf(buffer,"\"label\":\"%s\",\"unit\":\"%s\","
					"\"bits\":\"%d\",\"vref\":\"%.2f\",",
					pdata->psensdata->label,pdata->psensdata->units,
					pdata->psensdata->adc_bit,pdata->psensdata->adc_vrf);
				append_string_data(&pjson,&count,buffer);
				if(pdata->psensdata->datatype>0)
				{
					char format[STR_BUFF_SIZE];
					sprintf(format,"\"real\":\"%%.%df\",",
						pdata->psensdata->datatype);
					sprintf(buffer,format,pdata->realvalue);
				}
				else
				{
					sprintf(buffer,"\"real\":\"%d\",",(int)pdata->realvalue);
				}
				append_string_data(&pjson,&count,buffer);
			}
			sprintf(buffer,"\"aqidx\":\"%0.1f\",\"aqflg\":\"%d\",",
				pdata->idxvalue,pdata->idxvalid);
			append_string_data(&pjson,&count,buffer);
			sprintf(buffer,"\"vflag\":\"%d\",\"rflag\":\"%d\","
				"\"volt\":\"%.2f\"},",pdata->voltvalid,pdata->realvalid,
				pdata->voltvalue);
			append_string_data(&pjson,&count,buffer);
			pitem = pitem->next;
		}
		else
		{
			sprintf(buffer,"\"data%d\":{\"raw\":\"%d\"},",
				index,pnode->pdata[index]);
			append_string_data(&pjson,&count,buffer);
		}
		index++;
	}
	sprintf(buffer,"\"aqidx\":\"%0.1f\",\"aqflg\":\"%d\",",
		pnode->idxvalue,pnode->idxvalid);
	append_string_data(&pjson,&count,buffer);
	sprintf(buffer,"\"countD\":\"%d\"}",index);
	append_string_data(&pjson,&count,buffer);
	/** create websocket data */
	pwebs = create_websock_data(pbuffer,pcount,pjson);
	/** free temp heap */
	if(count>0) free(pjson);
	return pwebs;
}
/*----------------------------------------------------------------------------*/
static int send_node_info(struct mg_connection *conn, char *initmsg)
{
	unsigned char* pwebs = 0x0;
	int count = 0;
	list_lock(&nodes);
	/** create websocket data for system info, node count */
	create_node_json(&nodes,(char**)&pwebs,&count,initmsg);
	mg_write(conn, pwebs, count-1);
	if(count>0) free(pwebs);
	list_unlock(&nodes);
	return 0;
}
/*----------------------------------------------------------------------------*/
static void cast_websocket(unsigned char *pwebs, int count)
{
	list_lock(&conns);
	conns.curr = 0x0;
	while(list_iterate(&conns))
	{
		wsconn *pconn = (wsconn*) conns.curr->item;
		mg_write(pconn->pconn, pwebs, count-1);
	}
	list_unlock(&conns);
}
/*----------------------------------------------------------------------------*/
static int cast_node_info(wsnode* pnode)
{
	unsigned char* pwebs = 0x0;
	int count = 0;
	char buffer[128];
	sprintf(buffer,"{\"type\":\"newnode\",\"uniqid\":\"%d\",\"nodeid\":\"%d\","
		"\"sensid\":\"%d\",\"appsid\":\"%d\",\"dcount\":\"%d\"}",
		pnode->uniqid,pnode->nodeid,pnode->sensid,pnode->appsid,pnode->dcount);
	create_websock_data((char**)&pwebs, &count, buffer);
	cast_websocket(pwebs,count);
	if(count>0) free(pwebs);
	return 0;
}
/*----------------------------------------------------------------------------*/
static int cast_node_data(wsnode* pnode)
{
	unsigned char* pwebs = 0x0;
	int count = 0;
	create_data_json(pnode,(char**)&pwebs, &count);
	cast_websocket(pwebs,count);
	if(count>0) free(pwebs);
	return 0;
}
/*----------------------------------------------------------------------------*/
static int cast_node_dead(wsnode* pnode)
{
	unsigned char* pwebs = 0x0;
	int count = 0;
	char buffer[128];
	sprintf(buffer,"{\"type\":\"nodedown\",\"uniqid\":\"%d\"}",pnode->uniqid);
	create_websock_data((char**)&pwebs, &count, buffer);
	cast_websocket(pwebs,count);
	if(count>0) free(pwebs);
	return 0;
}
/*----------------------------------------------------------------------------*/
static int cast_show_opts(int show)
{
	unsigned char* pwebs = 0x0;
	int count = 0;
	char buffer[128];
	sprintf(buffer,"{\"type\":\"%s\"}",show?"showopts":"hideopts");
	create_websock_data((char**)&pwebs, &count, buffer);
	cast_websocket(pwebs,count);
	if(count>0) free(pwebs);
	return 0;
}
/*----------------------------------------------------------------------------*/
void dummy_data_xC8x33(wsnode* pnode)
{
	static int loop;
	int dummyid[] = { 0x1, 0x2, 0x3, 0x4, 0x5, 0x6 };
	int count = sizeof(dummyid)/sizeof(int);
	time_t currtime = time(0x0);
	pnode->uniqid = -1; pnode->nodeid = -1;
	pnode->sensid = -1; pnode->appsid = -1; pnode->dcount = 9;
	pnode->pdata = (xword*) &pnode->packet[XMESH_DATA_OFFSET];
	pnode->timestamp = *localtime(&currtime);
	sprintf(pnode->timestring,"%04d%02d%02d:%02d%02d%02d",
		1900 + pnode->timestamp.tm_year,(pnode->timestamp.tm_mon+1),
		pnode->timestamp.tm_mday,pnode->timestamp.tm_hour,
		pnode->timestamp.tm_min,pnode->timestamp.tm_sec);
	pnode->ptiny->am_type = AM_DATA2BASE;
	pnode->pmesh->org_addr = dummyid[loop++%count];
	pnode->psens->board_id = 0xC8;
	pnode->pmesh->app_id = 0x33;
	/* dummy wsnode info */
	pnode->ptiny->dst_addr = 0x7E;
	pnode->ptiny->am_group = 0x73;
	pnode->ptiny->length = (pnode->dcount*sizeof(xword))+
		XMESH_XMESH_SIZE+XMESH_XSERVEH_SIZE;
	pnode->countp = pnode->ptiny->length+XMESH_TINYOSH_SIZE;
	pnode->pmesh->src_addr = 0x0000;
	pnode->pmesh->seq_num = 0x0000;
	pnode->psens->packet_id = 0x00;
	pnode->psens->parent = 0x00;
	/* create random data? */
	pnode->pdata[0] = rand()%0x400;
	pnode->pdata[1] = rand()%0x400;
	pnode->pdata[2] = rand()%0x400;
	pnode->pdata[3] = rand()%0x400;
	pnode->pdata[4] = rand()%0x400;
	pnode->pdata[5] = rand()%0x400;
	pnode->pdata[6] = rand()%0x400;
	pnode->pdata[7] = rand()%0x400;
	pnode->pdata[8] = rand()%0x400;
}
/*----------------------------------------------------------------------------*/
static int websocket_connect_handler(const struct mg_connection *conn)
{
	int error = 0;
	if(conns.count>=MAX_WEBSOCK_COUNT)
		error--; /** connection rejected - send non-zero */
	if(flag_verbose)
	{
		struct mg_request_info *info =
			mg_get_request_info((struct mg_connection*)conn);
		unsigned int test = (unsigned int)info->remote_ip;
		printf_time(printf_req);
		printf_req("[INFO] Client {%d.%d.%d.%d:%d} Request! (%d)\n",
			(test&0xFF000000)>>24,(test&0xFF0000)>>16,(test&0xFF00)>>8,
			(test&0xFF),info->remote_port,error);
	}
	return error;
}
/*----------------------------------------------------------------------------*/
static void websocket_ready_handler(struct mg_connection *conn)
{
	char initmsg[128];
	if(flag_verbose)
	{
		struct mg_request_info *info = mg_get_request_info(conn);
		unsigned int test = (unsigned int)info->remote_ip;
		printf_time(printf_req);
		printf_req("[INFO] Client {%d.%d.%d.%d:%d} Accepted!\n",
			(test&0xFF000000)>>24,(test&0xFF0000)>>16,(test&0xFF00)>>8,
			(test&0xFF),info->remote_port);
	}
	/** send program info along! */
	sprintf(initmsg,"{\"name\":\"%s\",\"version\":\"%s\"}",
		MY1APP_PROGNAME,MY1APP_PROGVERS);
	/** send connection/node info! */
	send_node_info(conn,initmsg);
	/** add to connection list if not already in there */
	check_in_conns((void*)conn);
}
/*----------------------------------------------------------------------------*/
static void websocket_done_handler(struct mg_connection * conn)
{
	struct mg_request_info *info = mg_get_request_info(conn);
	unsigned int test = (unsigned int)info->remote_ip;
	if(flag_verbose)
	{
		printf_time(printf_log);
		printf_log("[INFO] Client {%d.%d.%d.%d:%d} ",
			(test&0xFF000000)>>24,(test&0xFF0000)>>16,
			(test&0xFF00)>>8,(test&0xFF),info->remote_port);
		my1item* pitem = find_conn(conn);
		if(pitem) printf_req("Disconnect!\n");
		else printf_req("Killed?!\n");
	}
	check_out_conns((void*)conn);
}
/*----------------------------------------------------------------------------*/
static int websocket_data_handler(struct mg_connection * conn, int bits,
	char *data, size_t data_len)
{
	struct mg_request_info *info = mg_get_request_info(conn);
	unsigned int test = (unsigned int)info->remote_ip;
	int result = 1; /** non-zero to keep going? */
	data[data_len] = 0x0; /** always extra 2 chars? */
	switch(bits&0x0F) /** check opcode! */
	{
		case 0x01:
		{
			/* text frame */
			char *tokenbuff, *ptoken;
			/* create separate token processing buffer */
			tokenbuff = malloc(data_len+1);
			strcpy(tokenbuff,data);
			ptoken = strtok(tokenbuff,TOKEN_DELIM);
			if(ptoken)
			{
				if(strcmp(ptoken,"nodeinfo")==0)
				{
					send_node_info(conn,0x0);
				}
				else if(strcmp(ptoken,"ping")==0)
				{
					/** update timestamp! */
					time_t currtime = time(0x0);
					my1item* pitem = find_conn(conn);
					if(pitem)
					{
						wsconn *pconn = (wsconn*) pitem->item;
						pconn->atime = *localtime(&currtime);
					}
					else
					{
						result = 0; /** close connection? */
					}
				}
				else
				{
					printf_time(printf_log);
					printf_log("[INFO] Client {%d.%d.%d.%d:%d} ",
						(test&0xFF000000)>>24,(test&0xFF0000)>>16,
						(test&0xFF00)>>8,(test&0xFF),info->remote_port);
					printf_log("UNKNOWN REQUEST {%s}(%d)\n",data,(int)data_len);
				}
			}
			/** ignore 'empty' messages */
			free(tokenbuff);
			break;
		}
		case 0x08:
			/** WILL NEVER GET HERE! Use websocket_done_handler! */
			break;
		default:
			printf_time(printf_log);
			printf_log("[INFO] Client {%d.%d.%d.%d:%d} ",
				(test&0xFF000000)>>24,(test&0xFF0000)>>16,
				(test&0xFF00)>>8,(test&0xFF),info->remote_port);
			printf_log("UNSUPPORTED WEBSOCKET DATA [%04X]{%s}(%d)\n",
				bits,data,(int)data_len);
			break;
	}
	return result;
}
/*----------------------------------------------------------------------------*/
struct mg_context* restart_websocket(struct mg_context* ctx,
	struct mg_callbacks* cbacks, const char** opts, int* started)
{
	if(*started)
	{
		/* stop websocket server */
		mg_stop(ctx);
		/* clean-up connection list */
		if(free_conns())
		{
			printf_time(printf_err);
			printf_err("[ERROR] Error closing connections!\n");
		}
		list_setup(&conns,LIST_TYPE_DEFAULT);
	}
	else
		*started = 1;
	/* startup the websocket server */
	return mg_start(cbacks, NULL, opts);
}
/*----------------------------------------------------------------------------*/
int expire_dead_node(void* pcheck)
{
	int isdead = 0;
	wsnode *pnode = (wsnode*) pcheck;
	time_t currtime = time(0x0);
	time_t datatime = mktime(&pnode->timestamp);
	if((int)difftime(currtime,datatime)>timeout*MINUTE_EXPIRE_S)
	{
		printf_time(printf_log);
		printf_log("[DEADNODE] Node_%04X_%02X_%02X removed!\n",
			pnode->nodeid,pnode->sensid,pnode->appsid);
		/* inform subscribers */
		cast_node_dead(pnode);
		/* free memory! */
		free(pcheck);
		/* schedule for delete */
		isdead = 1;
	}
	return isdead;
}
/*----------------------------------------------------------------------------*/
int expire_dead_conn(void* pcheck)
{
	int isdead = 0;
	wsconn *pconn = (wsconn*) pcheck;
	time_t currtime = time(0x0);
	time_t datatime = mktime(&pconn->atime);
	if((int)difftime(currtime,datatime)>CLIENT_TIME_OUT*MINUTE_EXPIRE_S)
	{
		unsigned int test = (unsigned int) pconn->pinfo->remote_ip;
		printf_time(printf_req);
		printf_req("[DEADCONN] Conn_%d.%d.%d.%d:%d removed!\n",
			(test&0xFF000000)>>24,(test&0xFF0000)>>16,
			(test&0xFF00)>>8,(test&0xFF), pconn->pinfo->remote_port);
		/* free memory! */
		free(pcheck);
		/* schedule for delete */
		isdead = 1;
	}
	return isdead;
}
/*----------------------------------------------------------------------------*/
void check_the_dead(void)
{
	/** look for dead node */
	list_lock(&nodes);
	list_browser(&nodes,&expire_dead_node,0x0);
	list_unlock(&nodes);
	/** look for dead conn as well */
	list_lock(&conns);
	list_browser(&conns,&expire_dead_conn,0x0);
	list_unlock(&conns);
}
/*----------------------------------------------------------------------------*/
int check_new_sens(char* psens)
{
	int test;
	char pdbtext[FILENAME_SIZE];
	/* load from text file */
	if(psens) strcpy(pdbtext,psens);
	else strcpy(pdbtext,DEFAULT_DB_FILETEXT);
	printf_req("[INFO] Loading board info in '%s'.\n",pdbtext);
	test = fopen_sens_list(&sensb,pdbtext);
	if(test==BOARD_ELOAD)
	{
		strcpy(pdbtext,DEFAULT_DB_FILEPATH);
		if(psens) strcat(pdbtext,psens);
		else strcat(pdbtext,DEFAULT_DB_FILETEXT);
		printf_req("[INFO] Looking in '%s'.\n",pdbtext);
		test = fopen_sens_list(&sensb,pdbtext);
	}
	if(test==BOARD_ELOAD)
		printf_req("[WARN] Cannot find board info file!\n");
	else if(test<0)
		printf_req("[WARN] Check board info in '%s'! (%d)\n", pdbtext,test);
	return test;
}
/*----------------------------------------------------------------------------*/
void print_info(FILE* pfile)
{
	/* print tool info */
	fprintf(pfile,"\n%s - %s (%s)\n",MY1APP_PROGNAME,MY1APP_PROGINFO,
		MY1APP_PROGVERS);
	fprintf(pfile,"  => by azman@my1matrix.net\n\n");
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	ASerialPort_t cPort;
	ASerialConf_t cConf;
	my1key_t key;
	int test, loop, dodata = 0, terminal = 1, baudrate = 57600;
	int scan = 0, dumbchk = 0, help = 0, serve = 1;
	int dodumb = 0, dobase = 0, dobeat = 0;
	int thisweek, dbname_len;
	int amgroup = -1, appid = -1;
	char *ptty = 0x0,*psens = 0x0;
	/** xmesh structure */
	tinyos_sframe tosframe;
	/** server stuff */
	struct mg_context *ctx;
	struct mg_callbacks callbacks;
	const char *options[] =
	{
		"listening_ports", "1337",
		"document_root", "wsnroot",
		"enable_directory_listing", "no",
		NULL
	};
	/** database stuff */
	char pdbname[FILENAME_SIZE],plogname[FILENAME_SIZE];
	char *pdbf = 0x0, *plog = 0x0;
	sqlite3 *pdbase;
	wsnode tnode;
	/* timer stuff */
	struct sigaction action;
	struct itimerval timer;

	/* print tool info */
	print_info(stdout);

	/* check program arguments */
	if(argc>1)
	{
		for(loop=1;loop<argc;loop++)
		{
			if(argv[loop][0]=='-') /* options! */
			{
				if(!strcmp(argv[loop],"--port"))
				{
					if(get_param_int(argc,argv,&loop,&test)<0)
					{
						printf("Cannot get port number - using default!\n");
					}
					else if(test<1||test>MAX_COM_PORT)
					{
						printf("Invalid port (%d)- using default!\n",test);
					}
					else terminal = test;
				}
				else if(!strcmp(argv[loop],"--tty"))
				{
					if(!(ptty=get_param_str(argc,argv,&loop)))
					{
						printf("Cannot get tty name - using default!\n");
					}
				}
				else if(!strcmp(argv[loop],"--baud"))
				{
					if(get_param_int(argc,argv,&loop,&test)<0)
					{
						printf("Cannot get baudrate - using default!\n");
					}
					else baudrate = test;
				}
				else if(!strcmp(argv[loop],"--name"))
				{
					if(!(pdbf=get_param_str(argc,argv,&loop)))
					{
						printf("Cannot get base name - using default!\n");
					}
				}
				else if(!strcmp(argv[loop],"--dbtext"))
				{
					if(!(psens=get_param_str(argc,argv,&loop)))
					{
						printf("Cannot get board DB name - using default!\n");
					}
				}
				else if(!strcmp(argv[loop],"--amgroup"))
				{
					if(get_param_int(argc,argv,&loop,&test)<0)
					{
						printf("Cannot get AM group - not filtering!\n");
					}
					else amgroup = test;
				}
				else if(!strcmp(argv[loop],"--appid"))
				{
					if(get_param_int(argc,argv,&loop,&test)<0)
					{
						printf("Cannot get App ID - not filtering!\n");
					}
					else appid = test;
				}
				else if(!strcmp(argv[loop],"--timeout"))
				{
					if(get_param_int(argc,argv,&loop,&test)<0)
					{
						printf("Cannot get timeout value - using default!\n");
					}
					else timeout = test;
				}
				else if(!strcmp(argv[loop],"--timechk"))
				{
					if(get_param_int(argc,argv,&loop,&test)<0)
					{
						printf("Cannot get timechk value - using default!\n");
					}
					else timechk = test;
				}
				else if(!strcmp(argv[loop],"--log"))
				{
					plog = plogname;
				}
				else if(!strcmp(argv[loop],"--dummy"))
				{
					dumbchk = DUMMY_TIMEOUT_S; dodumb = 1;
				}
				else if(!strcmp(argv[loop],"--noserve"))
				{
					serve = 0;
				}
				else if(!strcmp(argv[loop],"--verbose"))
				{
					flag_verbose = 1;
				}
				else if(!strcmp(argv[loop],"--beat"))
				{
					dobeat = 1;
				}
				else if(!strcmp(argv[loop],"--scan"))
				{
					scan = 1;
				}
				else if(!strcmp(argv[loop],"--help"))
				{
					help = 1;
				}
				else
				{
					printf("Unknown option '%s'!\n",argv[loop]);
				}
			}
			else /* not an option? */
			{
				printf("Unknown parameter %s!\n",argv[loop]);
			}
		}
	}

	/* check if user requested help */
	if(help)
	{
		about();
		return 0;
	}

	/* initialize port */
	initialize_serial(&cPort);
#ifndef DO_MINGW
	sprintf(cPort.mPortName,"/dev/ttyUSB"); /* default on linux? */
#endif
	/* check user requested to change port name */
	if(ptty) sprintf(cPort.mPortName,"%s",ptty);

	/* check if user requested a port scan */
	if(scan)
	{
		print_portscan(&cPort);
		return 0;
	}

	/* check log options */
	if(plog)
	{
		time_t currtime = time(0x0);
		struct tm timestamp = *localtime(&currtime);
		sprintf(plog,"%s-%04d%02d%02d%02d%02d%02d.log",
			MY1APP_PROGNAME,1900 + timestamp.tm_year,
			(timestamp.tm_mon+1),timestamp.tm_mday,
			timestamp.tm_hour,timestamp.tm_min,timestamp.tm_sec);
		plogfile = fopen(plog,"wt");
		if(!plogfile) plog = 0x0;
		else print_info(plogfile);
	}

	/* initialize lists - sensb done by setup_sens_list */
	list_setup(&conns,LIST_TYPE_DEFAULT);
	list_setup(&nodes,LIST_TYPE_DEFAULT);

	/* initialize server stuff */
	memset(&callbacks, 0, sizeof(callbacks));
	callbacks.websocket_connect = websocket_connect_handler;
	callbacks.websocket_ready = websocket_ready_handler;
	callbacks.websocket_data = websocket_data_handler;
	callbacks.websocket_done = websocket_done_handler;

	/* check dummy mode... this is a hack */
	if(dumbchk) goto no_port_init;

	/* try to prepare port with requested terminal */
	if(!terminal) terminal = find_serial(&cPort,0x0);
	if(!set_serial(&cPort,terminal))
	{
		printf("Cannot prepare port '%s%d'!\n\n",
			cPort.mPortName,COM_PORT(terminal));
		return ERROR_NO_PORT;
	}

	/* apply custom baudrate */
	if(baudrate)
	{
			get_serialconfig(&cPort,&cConf);
		switch(baudrate)
		{
			default: printf("Invalid baudrate (%d)! Using default!\n",baudrate);
			case 9600: cConf.mBaudRate = MY1BAUD9600; break;
			case 19200: cConf.mBaudRate = MY1BAUD19200; break;
			case 38400: cConf.mBaudRate = MY1BAUD38400; break;
			case 57600: cConf.mBaudRate = MY1BAUD57600; break;
			case 115200: cConf.mBaudRate = MY1BAUD115200; break;
			case 230400: cConf.mBaudRate = MY1BAUD230400; break;
			case 460800: cConf.mBaudRate = MY1BAUD460800; break;
			case 921600: cConf.mBaudRate = MY1BAUD921600; break;
		}
		set_serialconfig(&cPort,&cConf);
	}
	/* keep current baudrate for display */
	switch(cConf.mBaudRate)
	{
		default:
		case MY1BAUD9600: baudrate = 9600; break;
		case MY1BAUD19200: baudrate = 19200; break;
		case MY1BAUD38400: baudrate = 38400; break;
		case MY1BAUD57600: baudrate = 57600; break;
		case MY1BAUD115200: baudrate = 115200; break;
		case MY1BAUD230400: baudrate = 230400; break;
		case MY1BAUD460800: baudrate = 460800; break;
		case MY1BAUD921600: baudrate = 921600; break;
	}

	/* try opening port */
	printf_req("[INFO] Opening serial interface ... \n");
	if(!open_serial(&cPort))
	{
		printf_err("[ERROR] Cannot open port '%s%d'!\n\n",cPort.mPortName,
			COM_PORT(cPort.mPortIndex));
		return ERROR_GENERAL;
	}

	/* dummy mode skip point... this is a hack */
no_port_init:
	/* check database file name - use default if not specified */
	if(pdbf) strcpy(pdbname,pdbf);
	else strcpy(pdbname,DEFAULT_DB_FILENAME);
	dbname_len = 0; while(pdbname[dbname_len]) dbname_len++;
	thisweek = create_dbfilename(pdbname,0);
	pdbf = pdbname;
	/* try opening database file */
	printf_req("[INFO] Opening database ... \n");
	if(sqlite3_open(pdbf, &pdbase)!=SQLITE_OK)
	{
		printf("Cannot open database file '%s'!\n\n",pdbname);
		return ERROR_NO_BASE;
	}
	/* create default table if necessary! */
	printf_req("[INFO] Checking data table(s) ... \n");
	if(setup_database(pdbase)<0)
	{
		printf("Cannot create database table(s) in '%s'!\n\n",pdbname);
		return ERROR_NO_BASE;
	}

	/* load sensor board info */
	test = setup_sens_list(&sensb,pdbase);
	if(test<0) printf("[WARN] Cannot get board info in DB! (%d)\n",test);
	else if(test==0)
	{
		/** try read from file */
		test = check_new_sens(psens);
		if(test>0)
		{
			cp2db_sens_list(&sensb,pdbase);
			clean_sens_list(&sensb);
			setup_sens_list(&sensb,pdbase);
		}
	}

	/* show sensor board count if in verbose mode */
	printf_req("[INFO] Board(s) in DB: %d.\n",sensb.count);

	/* initialize tinyos buffer */
	tosframe.indexb = 0;

	/* initialize buffer node! */
	setup_wsnode(&tnode);

	/* startup the websocket server */
	if(serve)
	{
		printf_req("[INFO] Starting websocket server ... \n");
		ctx = mg_start(&callbacks, NULL, options);
	}

	/* timer_handler as the signal handler for SIGVTALRM */
	memset(&action, 0, sizeof (action));
	action.sa_handler = &timer_handler;
	/**sigaction(SIGVTALRM, &action, NULL);*/
	sigaction(SIGALRM, &action, NULL);

	/*  initial timer timeout */
	timer.it_value.tv_sec = TIMER_TIMEOUT_S;
	timer.it_value.tv_usec = 0;
	/* auto-reload interval? */
	timer.it_interval.tv_sec = TIMER_TIMEOUT_S;
	timer.it_interval.tv_usec = 0;
	/* start timer - count down? */
	timer_expire = timechk;
	dummy_expire = dumbchk;
	/**if(setitimer(ITIMER_VIRTUAL,&timer,NULL)<0)*/
	if(setitimer(ITIMER_REAL,&timer,NULL)<0)
		fprintf(stderr,"[WARNING] Cannot set timer!\n");

	/* start random number generator if in dummy mode */
	if(dumbchk) srand(time(0x0));

	/* clear input buffer */
	purge_serial(&cPort);

	/* say we're ready to go... */
	printf("\n%s",MESSAGE_START);
	fflush(stdout);

	/* start main loop */
	while(1)
	{
		key = get_keyhit();
		if(key!=KEY_NONE)
		{
			if(key==KEY_F1) /* show commands */
			{
				printf("\n------------\n");
				printf("Command Keys\n");
				printf("------------\n");
				printf("<F1>  - Show command keys\n");
				printf("<F2>  - Show current settings\n");
				printf("<F3>  - Toggle verbose mode\n");
				printf("<F4>  - Toggle dummynode/heartbeat\n");
				printf("<F10> - Expire Timer Value\n");
				printf("<ESC> - Exit this program\n\n");
			}
			else if(key==KEY_F2) /* show current settings */
			{
				printf("[INFO] Program: '%s'\n",MY1APP_PROGNAME);
				printf("[INFO] Serial Port: '%s%d'\n",cPort.mPortName,
						COM_PORT(cPort.mPortIndex));
				printf("[INFO] Timeout: %d m, Timechk: %d (%d) m\n",
						timeout,timechk,timer_expire);
				printf("[INFO] Baudrate: %d bps\n",baudrate);
				printf("[INFO] Heartbeat: '%s'!\n",dobeat?"SHOWN":"HIDDEN");
				printf("[INFO] Connection(s): %d {",conns.count);
				list_lock(&conns);
				conns.curr = 0x0;
				while(list_iterate(&conns))
				{
					wsconn *pconn = (wsconn*) conns.curr->item;
					unsigned int test = (unsigned int) pconn->pinfo->remote_ip;
					printf("{Conn_%d.%d.%d.%d:%d}",(test&0xFF000000)>>24,
						(test&0xFF0000)>>16,(test&0xFF00)>>8,(test&0xFF),
						pconn->pinfo->remote_port);
				}
				list_unlock(&conns);
				printf("}\n");
				printf("[INFO] Active Node(s): %d {",nodes.count);
				list_lock(&nodes);
				nodes.curr = 0x0;
				while(list_iterate(&nodes))
				{
					wsnode *ptest = (wsnode*) nodes.curr->item;
					printf("{Node_%04X_%02X_%02X[%04X]}",ptest->nodeid,
						ptest->sensid,ptest->appsid,ptest->uniqid);
				}
				list_unlock(&nodes);
				printf("}\n");
			}
			else if(key==KEY_F3) /* toggle verbose mode */
			{
				flag_verbose = !flag_verbose;
				printf("[COMMAND] Verbose mode '%s'!\n",
					flag_verbose?"ON":"OFF");
			}
			else if(key==KEY_F4)
			{
				if(dumbchk) /* check dummy mode */
				{
					dodumb = !dodumb;
					printf("[COMMAND] Dummy nodes '%s'!\n",
						dodumb?"ACTIVE":"HIBERNATING");
				}
				else /* toggle heart beat mode */
				{
					dobeat = !dobeat;
					printf("[COMMAND] Heartbeat packets: '%s'!\n",
						dobeat?"SHOWN":"HIDDEN");
				}
			}
			else if(key==KEY_F5) /* HIDDEN! reload sens info from text file */
			{
				printf("[COMMAND] Reload sensor board info... \n");
				/* clear data info from all nodes */
				list_lock(&nodes);
				nodes.curr = 0x0;
				while(list_iterate(&nodes))
				{
					wsnode *pnode = (wsnode*) nodes.curr->item;
					clean_wsnode(pnode); /* actually, just free wsdata */
				}
				list_unlock(&nodes);
				/* cleanup main list */
				clean_sens_list(&sensb);
				/* read new sensor info */
				test = check_new_sens(psens);
				if(test>0)
				{
					/* update database */
					cp2db_sens_list(&sensb,pdbase);
					clean_sens_list(&sensb);
					setup_sens_list(&sensb,pdbase);
					/* update data info on all nodes */
					list_lock(&nodes);
					nodes.curr = 0x0;
					while(list_iterate(&nodes))
					{
						wsnode *pnode = (wsnode*) nodes.curr->item;
						check_sensorboard(pnode);
					}
					list_unlock(&nodes);
					printf("[INFO] Sensor board info reloaded!\n");
				}
			}
			else if(key==KEY_F6) /* HIDDEN! show sensorboard info */
			{
				printf("[INFO] Board(s) in DB: %d.\n",sensb.count);
				printf("\n-----------\n");
				printf("Board Count: %d\n",sensb.count);
				printf("-----------\n");
				sensb.curr = 0x0;
				while(list_iterate(&sensb))
				{
					sboard *pboard = (sboard*) sensb.curr->item;
					printf("Board ID: %02X (%d)\n",pboard->boardid,
						pboard->datacount);
					pboard->sensdata.curr = 0x0; loop = 0;
					while(list_iterate(&pboard->sensdata))
					{
						sbdata *pdata = (sbdata*) pboard->sensdata.curr->item;
						printf("Data %d: Bits=%d, Vref=%f, Label='%s', "
							"Unit='%s', Convert='%s'\n",loop,pdata->adc_bit,
							pdata->adc_vrf,pdata->label,pdata->units,
							pdata->convert);
						loop++;
					}
				}
				printf("\n");
			}
			else if(key==KEY_F7) /* HIDDEN! new db/log @ reset serial */
			{
				if(dumbchk) /* check dummy mode */
				{
					if(!dobase)
					{
						printf("[COMMAND] Request for new DB file!\n");
						dobase = 1;
					}
				}
				else /* reset serial interface */
				{
					printf("[COMMAND] Reset Serial Interface...\n");
					/* close port */
					close_serial(&cPort);
					/* reset tinyos buffer */
					tosframe.indexb = 0;
					/* try to re-open port */
					if(!open_serial(&cPort))
					{
						/* if failed, try first port found... */
						terminal = find_serial(&cPort,0x0);
						if(terminal) cPort.mPortIndex = terminal;
						if(!open_serial(&cPort))
						{
							printf_err("[ERROR] Cannot open port '%s%d'!\n\n",
								cPort.mPortName,COM_PORT(cPort.mPortIndex));
						}
					}
				}
			}
			else if(key==KEY_F8) /* HIDDEN! clear remote client! */
			{
				printf("[COMMAND] Clearing remote client list... \n");
				/* clean-up connection list */
				if(free_conns())
				{
					printf_time(printf_err);
					printf_err("[ERROR] Error closing connections!\n");
				}
				list_setup(&conns,LIST_TYPE_DEFAULT);
			}
			else if(key==KEY_F9) /* HIDDEN! restart webserver */
			{
				printf("[COMMAND] Restarting WebSocket Server\n");
				ctx = restart_websocket(ctx,&callbacks,options,&serve);
				printf("[COMMAND] WebSocket Server Restarted!\n");
			}
			else if(key==KEY_F10) /* initiate timer expire execution */
			{
				printf("[COMMAND] Expiring Timer Value!\n");
				timer_expire = 0;
			}
			else if(key==KEY_F11) /* HIDDEN! hide opts on clients */
			{
				printf("[COMMAND] Hiding Options in Clients!\n");
				cast_show_opts(0);
			}
			else if(key==KEY_F12) /* HIDDEN! show opts on clients */
			{
				printf("[COMMAND] Showing Options in Clients!\n");
				cast_show_opts(1);
			}
			else if(key==KEY_ESCAPE)
			{
				break;
			}
		}
		/* check serial port for incoming data */
		if(serial_opened(&cPort)&&check_incoming(&cPort))
		{
			byte_t cTemp = get_byte_serial(&cPort);
			switch(check_inbyte(&tosframe,cTemp))
			{
				case XMESHF_CHECK_OVERFLOW:
				{
					/** buffer error! */
					printf_time(printf_err);
					printf_err("[ERROR] Serial Buffer Overflow\n");
					break;
				}
				case XMESHF_CHECK_COMPLETE:
				{
					/** check filter */
					if((amgroup!=-1&&amgroup!=tosframe.ptiny->am_group)||
						(appid!=-1&&appid!=tosframe.pmesh->app_id))
					{
						printf_time(printf_log);
						printf_log("[FILTERED] G:%d@A:%d => ",amgroup,appid);
						for(loop=0;loop<tosframe.countb;loop++)
							printf_log("[%02X]",tosframe.buffer[loop]);
						printf_log("\n");
						break;
					}
					frame2wsnode(&tnode,&tosframe);
					/** send for processing */
					dodata = 1;
					break;
				}
				case XMESHF_CHECK_CONTINUE:
				{
					/** do nothing! */
					break;
				}
				default:
				{
					/** frame errors! */
					printf_time(printf_err);
					printf_err("[ERROR] Invalid frame (%d)\n",tosframe.tag);
				}
			}
		}
		/** check if data available for processing */
		if(dodata)
		{
			/* processing based on type */
			if(tnode.ptiny->am_type==AM_DATA2BASE)
			{
				do
				{
					/* check for duplicate node data? */
					wsnode *pnode, *ptemp = find_node(&tnode);
					if(ptemp&&ptemp->timestring[0])
					{
						time_t currtime = mktime(&tnode.timestamp);
						time_t prevtime = mktime(&ptemp->timestamp);
						if(difftime(currtime,prevtime)<(double)DUP_DATA_TIME_S)
						{
							printf_time(printf_log);
							printf_log("[INFO] Duplicate data (Node:%d)\n",
								pnode->nodeid);
							break;
						}
					}
					/** database interface */
					if((test=check_database(pdbase,&tnode))<0)
					{
						printf_time(printf_err);
						printf_err("[DB_ERROR] {NODE:%02X,SENS:%02X} ",
							tnode.nodeid,tnode.sensid);
						for(loop=0;loop<tnode.countp;loop++)
							printf_err("[%02X]",tnode.packet[loop]);
						printf_err("\n");
						break;
					}
					/* log data */
					printf_req("[[%s]] ",tnode.timestring);
					if(test>0) printf_req("[DB_NEWNODE] ");
					else printf_req("[DB_SUCCESS] ");
					print_frame_wsn(&tnode,printf_req);
					/** broadcast new node? */
					pnode = copy_node(ptemp,&tnode);
					if(!ptemp) cast_node_info(pnode);
					/** calculate data & save to db if applicable */
					if(calc_node_data(pnode)) calc2_database(pdbase,pnode);
					/** send to all connections */
					cast_node_data(pnode);
				}
				while(0); /* dummy loop */
			}
			else if(tnode.ptiny->am_type==AM_HEARTBEAT)
			{
				if(dobeat)
				{
					printf_req("[[%s]] [HEARTBEAT] [%04X]\n",
						tnode.timestring,tnode.pdata[0]);
				}
			}
			else if(tnode.ptiny->am_type==AM_HEALTH)
			{
				printf_log("[[%s]] [HEALTH] ",tnode.timestring);
				print_frame_wsn(&tnode,printf_log);
			}
			else
			{
				flag_display = 1;
				/** just log everything else - if requested */
				printf_log("[[%s]] [IGNORED] ",tnode.timestring);
				print_frame_wsn(&tnode,printf_log);
				flag_display = 0;
			}
			dodata = 0;
		}
		/** check for inactive nodes */
		if(timer_expire==0)
		{
			char timestring[DATETIME_FORMAT_LEN];
			time_t currtime = time(0x0);
			struct tm timestamp = *localtime(&currtime);
			/** reload as soon as possible! */
			timer_expire = timechk;
			sprintf(timestring,DATETIME_FORMAT,
				(1900+timestamp.tm_year),(timestamp.tm_mon+1),timestamp.tm_mday,
				timestamp.tm_hour,timestamp.tm_min,timestamp.tm_sec);
			printf_req("[[%s]] [TIMEOUT]\n",timestring);
			/** check dead nodes/conns */
			check_the_dead();
			/** check the need to change database file */
			/** mondays or first day of the month @0000-0030 */
			if(((timestamp.tm_wday==1||timestamp.tm_mday==1)&&
				timestamp.tm_hour==00&&timestamp.tm_min<=30)||dobase)
			{
				int chk_week = get_month_week(&timestamp);
				if(chk_week!=thisweek||dobase)
				{
					my1list temps;
					/* announce closure */
					printf_log("[[%s]] ",timestring);
					printf_log("[INFO] Closing DB file '%s'!\n",pdbname);
					/** copy node info from old db */
					setup_node_list(&temps,pdbase);
					/* close database */
					sqlite3_close(pdbase);
					/* create new file */
					pdbname[dbname_len] = 0x0;
					printf_log("[INFO] Creating new DB file!\n");
					thisweek = create_dbfilename(pdbname,dobase);
					/* try opening database file */
					if(sqlite3_open(pdbname, &pdbase)!=SQLITE_OK)
					{
						printf_err("[[%s]] ",timestring);
						printf_err("[ERROR] Failed opening '%s'!\n",pdbname);
						break; /* this will exit main loop! */
					}
					/* create default table if necessary! */
					if(setup_database(pdbase)<0)
					{
						printf_err("[[%s]] ",timestring);
						printf_err("[ERROR] Setup error for '%s'!\n",pdbname);
						break; /* this will exit main loop! */
					}
					/** copy board info to new db */
					cp2db_sens_list(&sensb,pdbase);
					/** copy node info to new db */
					clone_node_list(&temps,pdbase);
					/** clean up */
					clean_node_list(&temps);
					/** we're ready to go */
					printf_log("[INFO] Created DB file '%s'!\n",pdbname);
					/** renew log if log was requested */
					if(plog)
					{
						printf_time(printf_log);
						printf_log("[INFO] Log Stopped.\n");
						fclose(plogfile);
						sprintf(plog,"%s-%04d%02d%02d%02d%02d%02d.log",
							MY1APP_PROGNAME,1900 + timestamp.tm_year,
							(timestamp.tm_mon+1),timestamp.tm_mday,
							timestamp.tm_hour,timestamp.tm_min,timestamp.tm_sec);
						printf("[INFO] Creating a new log file... ");
						plogfile = fopen(plogname,"wt");
						if(!plogfile)
						{
							plog = 0x0;
							printf("failed!\n");
						}
						else
						{
							print_info(plogfile);
							printf("success. ('%s')!\n",plog);
						}
					}
				}
				dobase = 0;
			}
		}
		/** if in dummy mode, randomly send data */
		if(dumbchk&&dummy_expire==0)
		{
			if(dodumb)
			{
				dummy_data_xC8x33(&tnode);
				/** queue dummy data */
				dodata = 1;
			}
			/** reset dummy timer */
			dummy_expire = dumbchk;
		}
	}
	/** print ending message */
	printf("\nTerminating! May need to wait for socket to close...");
	fflush(stdout);
	/* cleanup sensor board info */
	clean_sens_list(&sensb);
	/* cleanup data structures - also cleans up list */
	free_conns();
	free_nodes();
	/* stop websocket server */
	if(serve) mg_stop(ctx);
	/* close database */
	sqlite3_close(pdbase);
	/* close port */
	close_serial(&cPort);
	printf(" done!\n");
	/* check log options */
	if(plog)
	{
		printf_time(printf_log);
		printf_log("[INFO] Log Terminated!\n");
		fclose(plogfile);
	}

	return 0;
}
/*----------------------------------------------------------------------------*/
