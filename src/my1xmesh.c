/*----------------------------------------------------------------------------*/
#include "my1xmesh.h"
/*----------------------------------------------------------------------------*/
/**int strip_sframe(tinyos_sframe* pframe);*/
/**int check_sframe(tinyos_sframe* pframe);*/
/*----------------------------------------------------------------------------*/
xword xcrc_byte(xword xcrc, xbyte data)
{
	int loop;
	xcrc = xcrc ^ data << 8;
	for(loop=0;loop<8;loop++)
	{
		if(xcrc & 0x8000) xcrc = xcrc << 1 ^ 0x1021; /** CRC-16-CCITT? */
		else xcrc = xcrc << 1;
	}
	return xcrc;
}
/*----------------------------------------------------------------------------*/
int strip_sframe(tinyos_sframe* pframe)
{
	int loop, type = 0;
	xword calccrc = 0, *pchkcrc;
	xbyte bbuff, *pbuff = pframe->buffer;
	/* check sync frame */
	if(pbuff[0]!=XMESHF_BYTE_SYNC||pbuff[pframe->indexb-1]!=XMESHF_BYTE_SYNC)
		return XMESHF_ERROR_NOSYNC;
	/* separate type byte and include in crc calc */
	type = pbuff[1];
	calccrc = xcrc_byte(calccrc,pbuff[1]);
	/* prepare to copy to 'processed' packet */
	pframe->countp = 0;
	/* loop through & copy - strip sync and escaped bytes */
	for(loop=2;loop<pframe->indexb-1;loop++)
	{
		if(pbuff[loop]!=XMESHF_BYTE_ESCP||pbuff[loop-1]==XMESHF_BYTE_ESCP)
		{
			bbuff = pbuff[loop];
			if(pbuff[loop-1]==XMESHF_BYTE_ESCP) bbuff ^= 0x20;
			pframe->packet[pframe->countp++] = bbuff;
		}
	}
	/* length minus 2 crc bytes */
	pframe->countp -= 2;
	/* calculate crc after all escaped bytes are stripped! */
	pbuff = pframe->packet;
	for(loop=0;loop<pframe->countp;loop++)
		calccrc = xcrc_byte(calccrc,pbuff[loop]);
	/* compare crc */
	pchkcrc = (xword*) &pbuff[loop];
	if(calccrc!=*pchkcrc)
		return XMESHF_ERROR_CRCCHK;
	/* return packet frame type */
	return type;
}
/*----------------------------------------------------------------------------*/
int check_sframe(tinyos_sframe* pframe)
{
	/* keep total bytes of raw data */
	pframe->countb = pframe->indexb;
	/* pre-process buffer into packet */
	pframe->tag = strip_sframe(pframe);
	/* sync & crc error filtered! */
	if(pframe->tag<0) return pframe->tag;
	/* check packet type */
	switch(pframe->tag)
	{
		case 0x42: /* packet frame without ack P_PACKET_NO_ACK */
			break;
		case 0x41: /* packet frame with ack request P_PACKET_ACK */
		case 0x40: /* ack packet frame P_ACK */
		case 0xFF: /* unknown packet */
		default:
			pframe->tag = XMESHF_ERROR_NOTYPE;
			return pframe->tag;
	}
	/* check minimum size */
	if(pframe->countp<XMESH_TINYOSH_SIZE)
	{
		pframe->tag = XMESHF_ERROR_SIZE_H;
		return pframe->tag;
	}
	/* check payload size */
	pframe->ptiny = (tinyos_header*) &pframe->packet[0];
	if(pframe->ptiny->length!=pframe->countp-XMESH_TINYOSH_SIZE)
	{
		pframe->tag = XMESHF_ERROR_SIZE_P;
		return pframe->tag;
	}
	/* track header size */
	pframe->tag = XMESH_TINYOSH_SIZE;
	/* mesh header may not be available!? specs says 0/7 */
	if(pframe->ptiny->length>XMESH_XMESH_SIZE+XMESH_XSERVEH_SIZE)
	{
		pframe->pmesh = (xmesh_header*) &pframe->packet[pframe->tag];
		pframe->tag += XMESH_XMESH_SIZE;
	}
	else pframe->pmesh = 0x0;
	/* look for xsense header */
	if(pframe->ptiny->length>XMESH_XSERVEH_SIZE)
	{
		pframe->psens = (xsense_header*) &pframe->packet[pframe->tag];
		pframe->tag += XMESH_XSERVEH_SIZE;
	}
	else pframe->psens = 0x0;
	/* everything checks out - put a time stamp */
	{
		time_t currtime = time(0x0);
		pframe->timestamp = *localtime(&currtime);
	}
	return pframe->tag; /* holds data index! */
}
/*----------------------------------------------------------------------------*/
int check_inbyte(tinyos_sframe* pframe, xbyte ibyte)
{
	int result = XMESHF_CHECK_CONTINUE;
	/* waiting for sync? */
	if(pframe->indexb==0)
	{
		if(ibyte==XMESHF_BYTE_SYNC)
		{
			pframe->buffer[pframe->indexb++] = ibyte;
		}
		/* ignore byte if anything else */
	}
	else
	{
		/* should put sync time-out? */
		if(ibyte==XMESHF_BYTE_SYNC)
		{
			/* in case should be init sync! */
			if(pframe->indexb==1) return result;
			/* accumulate raw byte */
			pframe->buffer[pframe->indexb++] = ibyte;
			/* validate packet */
			if(check_sframe(pframe)<0) result = pframe->tag;
			else result = XMESHF_CHECK_COMPLETE;
			/* reset buffer index */
			pframe->indexb = 0;
		}
		else
		{
			/* fill the buffer */
			pframe->buffer[pframe->indexb++] = ibyte;
			/* check buffer overflow! */
			if(pframe->indexb>=XMESHF_PACKET_MAXBYTE)
			{
				pframe->indexb = 0;
				result = XMESHF_CHECK_OVERFLOW;
			}
		}
	}
	return result;
}
/*----------------------------------------------------------------------------*/
