/*----------------------------------------------------------------------------*/
#include "my1wsock.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
char* append_string_data(char **pbuffer, int *pcount, char *pdata)
{
	int count,total;
	char *ptest;
	count = strlen(pdata);
	if((*pcount)==0) count++;
	total = (*pcount) + count;
	ptest = realloc(*pbuffer, total);
	if(ptest)
	{
		if((*pcount)==0) ptest[0] = 0x0;
		strcat(ptest,pdata);
		*pbuffer = ptest;
		*pcount = total;
	}
	return ptest;
}
/*----------------------------------------------------------------------------*/
char* create_websock_data(char **pbuffer, int *pcount, char *pdata)
{
	char *ptest, buffer[DEFAULT_WSHEADER_SIZE];
	int index = 0, count = strlen(pdata), loop;
	buffer[index++] = 0x81; /* FIN, text */
	if(count<126)
	{
		buffer[index++] = (count&0xFF);
	}
	else if(count<65536)
	{
		buffer[index++] = 0x7e; /* 126 */
		buffer[index++] = (count&0xFF00)>>8;
		buffer[index++] = (count&0xFF);
	}
	else /* uint64_t? but MSB must be zero? */
	{
		buffer[index++] = 0x7f; /* 127 */
		buffer[index++] = 0x00; /* assume count is 32-bit? */
		buffer[index++] = 0x00;
		buffer[index++] = 0x00;
		buffer[index++] = 0x00;
		buffer[index++] = (count&0xFF000000)>>24;
		buffer[index++] = (count&0xFF0000)>>16;
		buffer[index++] = (count&0xFF00)>>8;
		buffer[index++] = (count&0xFF);
	}
	buffer[index++] = 0x0;
	count += index;
	ptest = realloc(*pbuffer,count);
	if(ptest)
	{
		*pbuffer = ptest;
		*pcount = count;
		for(loop=0;loop<index-1;loop++)
			ptest[loop] = buffer[loop];
		sprintf(&ptest[loop],"%s",pdata);
	}
	return ptest;
}
/*----------------------------------------------------------------------------*/
