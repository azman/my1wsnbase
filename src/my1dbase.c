/*----------------------------------------------------------------------------*/
#include "my1dbase.h"
#include <stdio.h> /* sprintf */
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void free_my1value(void* pvalue)
{
	free(pvalue);
}
/*----------------------------------------------------------------------------*/
void setup_sbdata(sbdata* pdata)
{
	pdata->uniqid = -1;
	pdata->pequ = 0x0;
	list_setup(&pdata->params,LIST_TYPE_DEFAULT);
}
/*----------------------------------------------------------------------------*/
void clean_sbdata(sbdata* pdata)
{
	if(pdata->pequ)
	{
		free_expression(pdata->pequ);
		free((void*)pdata->pequ);
	}
	list_clean(&pdata->params,&free_my1value);
}
/*----------------------------------------------------------------------------*/
void free_sbdata(void* pbdata)
{
	clean_sbdata((sbdata*)pbdata);
	free(pbdata);
}
/*----------------------------------------------------------------------------*/
void setup_sboard(sboard* psens)
{
	psens->boardid = -1;
	psens->datacount = -1;
	list_setup(&psens->sensdata,LIST_TYPE_DEFAULT);
}
/*----------------------------------------------------------------------------*/
void clean_sboard(sboard* psens)
{
	list_clean(&psens->sensdata,&free_sbdata);
}
/*----------------------------------------------------------------------------*/
void free_sboard(void* pboard)
{
	clean_sboard((sboard*)pboard);
	free(pboard);
}
/*----------------------------------------------------------------------------*/
#define DATA_DELIM "{},=\n\r"
/*----------------------------------------------------------------------------*/
int fopen_sens_list(my1list *plist, char *pdbtext)
{
	sboard *ptest = 0x0; sbdata *ptemp = 0x0;
	char *readbuff = 0x0, *ptoken, *pcheck;
	int count, readcount, buffcount;
	int dindex, dcount = -1, sensid = -1, gofish = 0, error = BOARD_OK;
	FILE *psensfile;

	list_setup(plist,LIST_TYPE_DEFAULT);

	psensfile = fopen(pdbtext,"r");
	if(!psensfile) return BOARD_ELOAD;

	while(1)
	{
		char *tempbuff = 0x0;
		readcount = getline(&tempbuff,(size_t*)&buffcount,psensfile);
		if(readcount==-1) break;
		/* create separate processing buffer */
		ptoken = realloc(readbuff,readcount+1);
		readbuff = ptoken; /** assume always success */
		count = readcount;
		strcpy(readbuff,tempbuff);
		free(tempbuff);
		/* newline character was included in tempbuff! */
		ptoken[count-1] = 0x0;
		count--;
		/** always look for new board statment */
		if(ptoken[0]=='['&&ptoken[count-1]==']')
		{
			if(ptest) { free_sboard(ptest); error = BOARD_EFORMAT; break; }
			sensid = -1; dcount = -1;
			if(!gofish) gofish = 1;
			/* create board */
			ptest = (sboard*) malloc(sizeof(sboard));
			setup_sboard(ptest);
		}
		else if(gofish)
		{
			/** then look for board info or data */
			ptoken = strtok(readbuff,DATA_DELIM);
			while(ptoken)
			{
				pcheck = strtok(0x0,DATA_DELIM);
				if(sensid<0||dcount<0)
				{
					if(strcmp(ptoken,"SensId")==0)
					{
						if(!pcheck) { error = BOARD_EDVAL_MISS; break; }
						sensid = atoi(pcheck);
					}
					else if(strcmp(ptoken,"CountD")==0)
					{
						if(!pcheck) { error = BOARD_EDVAL_MISS; break; }
						dcount = atoi(pcheck);
						dindex = 0;
					}
					else
					{
						error = BOARD_EINFO;
						break;
					}
					/* must not have anything else */
					pcheck = strtok(0x0,DATA_DELIM);
					if(pcheck) { error = BOARD_EINFO_EXTRA; break; }
				}
				else if(dindex<dcount)
				{
					/* look for data */
					char ttext[16];
					sprintf(ttext,"Data%d",dindex);
					if(strcmp(ptoken,ttext)==0)
					{
						dindex++;
						ptemp = (sbdata*) malloc(sizeof(sbdata));
						setup_sbdata(ptemp);
						do {
							/* should strip pcheck... just in case??? */
							if(!pcheck||strcmp(pcheck,"DataType")!=0)
							{ error = BOARD_EDATA_MISS; break; }
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck) { error = BOARD_EDVAL_MISS; break; }
							ptemp->datatype = atoi(pcheck);
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck||strcmp(pcheck,"ADCBit")!=0)
							{ error = BOARD_EDATA_MISS; break; }
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck) { error = BOARD_EDVAL_MISS; break; }
							ptemp->adc_bit = atoi(pcheck);
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck||strcmp(pcheck,"ADCVrf")!=0)
							{ error = BOARD_EDATA_MISS; break; }
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck) { error = BOARD_EDVAL_MISS; break; }
							ptemp->adc_vrf = atof(pcheck);
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck||strcmp(pcheck,"LabelD")!=0)
							{ error = BOARD_EDATA_MISS; break; }
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck) { error = BOARD_EDVAL_MISS; break; }
							count = strlen(pcheck);
							if(count<LABEL_MAX_CHAR-1&&
								pcheck[0]=='"'&&pcheck[count-1]=='"')
							{
								pcheck[count-1] = 0x0;
								strcpy(ptemp->label,&pcheck[1]);
							}
							else ptemp->label[0] = 0x0;
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck||strcmp(pcheck,"UnitsL")!=0)
							{ error = BOARD_EDATA_MISS; break; }
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck) { error = BOARD_EDVAL_MISS; break; }
							count = strlen(pcheck);
							if(count<UNITS_MAX_CHAR-1&&
								pcheck[0]=='"'&&pcheck[count-1]=='"')
							{
								pcheck[count-1] = 0x0;
								strcpy(ptemp->units,&pcheck[1]);
							}
							else ptemp->units[0] = 0x0;
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck||strcmp(pcheck,"MathEq")!=0)
							{ error = BOARD_EDATA_MISS; break; }
							pcheck = strtok(0x0,DATA_DELIM);
							if(!pcheck) { error = BOARD_EDVAL_MISS; break; }
							count = strlen(pcheck);
							if(count>0&&count<CONVERT_MAX_CHAR-1&&
								pcheck[0]=='"'&&pcheck[count-1]=='"')
							{
								pcheck[count-1] = 0x0;
								strcpy(ptemp->convert,&pcheck[1]);
								ptemp->pequ = parse_equation(ptemp->convert);
							}
							else ptemp->convert[0] = 0x0;
							/* must not have anything else */
							pcheck = strtok(0x0,DATA_DELIM);
							if(pcheck) { error = BOARD_EINFO_EXTRA; break; }
						} while(0);
						if(error) break;
						/* create parameters - raw value and real value */
						my1value *praw = (my1value*) malloc(sizeof(my1value));
						value4f(praw,0.0);
						list_push_item(&ptemp->params,(void*)praw);
						my1value *pval = (my1value*) malloc(sizeof(my1value));
						value4f(pval,0.0);
						list_push_item(&ptemp->params,(void*)pval);
						/** assign to equations */
						if(ptemp->pequ)
							par4_expression(ptemp->pequ,&ptemp->params);
						/* add sensor data info to board */
						list_push_item(&ptest->sensdata,(void*)ptemp);
						/* already assigned - avoid deletion! */
						ptemp = 0x0;
					}
					else
					{
						error = BOARD_EDATA_NONE;
						break;
					}
				}
				ptoken = pcheck;
			}
			if(error) break;
			if(dindex==dcount)
			{
				/* got full board info, put into list */
				ptest->boardid = sensid;
				ptest->datacount = dcount;
				list_push_item(plist,(void*)ptest);
				/* already assigned - avoid deletion! */
				ptest = 0x0;
			}
		}
	}
	free(readbuff); /* null check NOT needed - man malloc! */
	fclose(psensfile);
	if(ptemp) free_sbdata(ptemp);
	if(ptest) free_sboard(ptest);
	return error == BOARD_OK ? plist->count : error;
}
/*----------------------------------------------------------------------------*/
int setup_sens_list(my1list* plist,sqlite3* pdbase)
{
	list_setup(plist,LIST_TYPE_DEFAULT);
	{
		/** first get all boards */
		sqlite3_stmt *pstate;
		char pquery[] = "SELECT SensId,CountD FROM my1sens;";
		if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
		{
			while(sqlite3_step(pstate)==SQLITE_ROW)
			{
				sboard *ptest = (sboard*) malloc(sizeof(sboard));
				setup_sboard(ptest);
				ptest->boardid = sqlite3_column_int(pstate,0);
				ptest->datacount = sqlite3_column_int(pstate,1);
				list_push_item(plist,(void*)ptest);
			}
			sqlite3_finalize(pstate);
		}
		else
		{
#ifdef MY1DEBUG
			printf("Cannot prepare '%s'\n\n",pquery);
#endif
			return QUERY_ERROR_PREPARE;
		}
	}
	/** then get all data columns info for all boards */
	plist->curr = 0x0;
	while(list_iterate(plist))
	{
		sqlite3_stmt *pstate;
		char pquery[] = "SELECT IndexD,DataType,ADCBit,ADCVrf,"
			"LabelD,UnitsL,MathEq,UniqId FROM my1sensdata "
			"WHERE SensId=?1 ORDER BY IndexD;";
		if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
		{
			char *ptext;
			int index = 0, tsize;
			sboard *ptest = (sboard*) plist->curr->item;
			if(sqlite3_bind_int(pstate,1,ptest->boardid)!=SQLITE_OK)
			{
#ifdef MY1DEBUG
				printf("Cannot bind first parameter in '%s'\n\n",pquery);
#endif
				continue;
			}
			while(sqlite3_step(pstate)==SQLITE_ROW)
			{
				int indexdb = sqlite3_column_int(pstate,0);
				if(indexdb!=index)
				{
#ifdef MY1DEBUG
					printf("Invalid Index %d (NOT %d)\n\n",indexdb,index);
#endif
					break;
				}
				/* create sbdata */
				sbdata *ptemp = (sbdata*) malloc(sizeof(sbdata));
				setup_sbdata(ptemp);
				/* get data type */
				ptemp->datatype = sqlite3_column_int(pstate,1);
				/* get adc bits */
				ptemp->adc_bit = sqlite3_column_int(pstate,2);
				/* pre-calculate maxvalue! */
				ptemp->adc_max = (1 << ptemp->adc_bit);
				/* get adc vref */
				ptemp->adc_vrf = sqlite3_column_double(pstate,3);
				/* get data label */
				ptext = (char*) sqlite3_column_text(pstate,4);
				tsize = sqlite3_column_bytes(pstate,4);
				if(tsize<LABEL_MAX_CHAR-1)
					strcpy(ptemp->label,ptext);
				else
					ptemp->label[0] = 0x0;
				/* get measurement unit label */
				ptext = (char*) sqlite3_column_text(pstate,5);
				tsize = sqlite3_column_bytes(pstate,5);
				if(tsize<UNITS_MAX_CHAR-1)
					strcpy(ptemp->units,ptext);
				else
					ptemp->units[0] = 0x0;
				/* get math equation string */
				ptext = (char*) sqlite3_column_text(pstate,6);
				tsize = sqlite3_column_bytes(pstate,6);
				if(tsize>0&&tsize<CONVERT_MAX_CHAR-1)
				{
					strcpy(ptemp->convert,ptext);
					ptemp->pequ = parse_equation(ptemp->convert);
				}
				else
					ptemp->convert[0] = 0x0;
				/* create containers - raw value and real value */
				my1value *praw = (my1value*) malloc(sizeof(my1value));
				value4f(praw,0.0);
				list_push_item(&ptemp->params,(void*)praw);
				my1value *pval = (my1value*) malloc(sizeof(my1value));
				value4f(pval,0.0);
				list_push_item(&ptemp->params,(void*)pval);
				if(ptemp->pequ)
					par4_expression(ptemp->pequ,&ptemp->params);
				/* get data uniqid */
				ptemp->uniqid = sqlite3_column_int(pstate,7);
				/* add sensor data info to board */
				list_push_item(&ptest->sensdata,(void*)ptemp);
				index++;
			}
			sqlite3_finalize(pstate);
		}
		else
		{
#ifdef MY1DEBUG
			printf("Cannot prepare '%s'\n\n",pquery);
#endif
			return QUERY_ERROR_PREPARE;
		}
	}
	return plist->count;
}
/*----------------------------------------------------------------------------*/
int cp2db_sens_list(my1list* plist,sqlite3* pdbase)
{
	int result = QUERY_OK;
	/** remove all board info */
	{
		/* remove board data info */
		sqlite3_stmt *pstate;
		do {
			char pquery[] = "DELETE FROM my1sensdata;";
			if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
				result = QUERY_ERROR_PREPARE; break; }
			if(sqlite3_step(pstate)!=SQLITE_DONE)
				result = QUERY_ERROR_EXECUTE;
		} while(0);
		sqlite3_finalize(pstate);
		if(result!=QUERY_OK) return result;
	}
	{
		/* remove board general info */
		sqlite3_stmt *pstate;
		do {
			char pquery[] = "DELETE FROM my1sens;";
			if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
				result = QUERY_ERROR_PREPARE; break; }
			if(sqlite3_step(pstate)!=SQLITE_DONE)
				result = QUERY_ERROR_EXECUTE;
		} while(0);
		sqlite3_finalize(pstate);
		if(result!=QUERY_OK) return result;
	}
	/** update all boards - rewrite into database */
	plist->curr = 0x0;
	while(list_iterate(plist))
	{
		result = QUERY_OK;
		sqlite3_stmt *pstate; int index = 0;
		sboard *ptest = (sboard*) plist->curr->item;
		/** write data info first */
		ptest->sensdata.curr = 0x0;
		while(list_iterate(&ptest->sensdata))
		{
			sbdata *ptemp = ptest->sensdata.curr->item;
			char pquery[] = "INSERT INTO my1sensdata (SensId,IndexD,DataType,"
				"ADCBit,ADCVrf,LabelD,UnitsL,MathEq) VALUES "
				"(?1,?2,?3,?4,?5,?6,?7,?8);";
			do {
				int size;
				if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
					result = QUERY_ERROR_PREPARE; break; }
				if(sqlite3_bind_int(pstate,1,ptest->boardid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,2,index)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,3,ptemp->datatype)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,4,ptemp->adc_bit)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_double(pstate,5,ptemp->adc_vrf)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				size = strlen(ptemp->label);
				if(sqlite3_bind_text(pstate,6,(const char*)ptemp->label,
					size,SQLITE_TRANSIENT)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				size = strlen(ptemp->units);
				if(sqlite3_bind_text(pstate,7,(const char*)ptemp->units,
					size,SQLITE_TRANSIENT)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				size = strlen(ptemp->convert);
				if(sqlite3_bind_text(pstate,8,(const char*)ptemp->convert,
					size,SQLITE_TRANSIENT)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_step(pstate)!=SQLITE_DONE)
					result = QUERY_ERROR_EXECUTE;
			} while(0);
			sqlite3_finalize(pstate);
			if(result!=QUERY_OK) break;
			/* confirm board data uniqid */
			char pqueryC[] = "SELECT UniqId FROM my1sensdata "
					"WHERE SensId=?1 and IndexD=?2;";
			do {
				if(sqlite3_prepare_v2(pdbase,pqueryC,-1,&pstate,0)!=SQLITE_OK) {
					result = QUERY_ERROR_PREPARE; break; }
				if(sqlite3_bind_int(pstate,1,ptest->boardid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,2,index)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_step(pstate)!=SQLITE_ROW) {
					result = QUERY_ERROR_EXECUTE; break; }
				ptemp->uniqid = sqlite3_column_int(pstate,0);
			} while(0);
			sqlite3_finalize(pstate);
			if(result!=QUERY_OK) break;
			index++;
		}
		/** now, we write board info, if no missing data */
		if(index==ptest->datacount)
		{
			char pquery[] = "INSERT INTO my1sens (SensId,CountD) "
				"VALUES (?1,?2);";
			do {
				if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
					result = QUERY_ERROR_PREPARE; break; }
				if(sqlite3_bind_int(pstate,1,ptest->boardid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,2,ptest->datacount)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_step(pstate)!=SQLITE_DONE)
					result = QUERY_ERROR_EXECUTE;
			} while(0);
			sqlite3_finalize(pstate);
		}
		/* remove partially found data */
		if(result!=QUERY_OK)
		{
			do {
				char pquery[] = "DELETE FROM my1sensdata WHERE SensId=?1;";
				if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
					result = QUERY_ERROR_PREPARE; break; }
				if(sqlite3_bind_int(pstate,1,ptest->boardid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_step(pstate)!=SQLITE_DONE)
					result = QUERY_ERROR_EXECUTE;
			} while(0);
			sqlite3_finalize(pstate);
			do {
				char pquery[] = "DELETE FROM my1sens WHERE SensId=?1;";
				if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
					result = QUERY_ERROR_PREPARE; break; }
				if(sqlite3_bind_int(pstate,1,ptest->boardid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_step(pstate)!=SQLITE_DONE)
					result = QUERY_ERROR_EXECUTE;
			} while(0);
			sqlite3_finalize(pstate);
			break;
		}
	}
	return result;
}
/*----------------------------------------------------------------------------*/
int clean_sens_list(my1list* plist)
{
	list_clean(plist,&free_sboard);
	return plist->count;
}
/*----------------------------------------------------------------------------*/
sboard* check_sens_list(my1list* plist, int boardid)
{
	sboard *pboard = 0x0, *pcheck;
	plist->curr = 0x0;
	while(list_iterate(plist))
	{
		pcheck = (sboard*) plist->curr->item;
		if(pcheck->boardid==boardid)
		{
			pboard = pcheck;
			break;
		}
	}
	return pboard;
}
/*----------------------------------------------------------------------------*/
void setup_wsnode(wsnode* pnode)
{
	pnode->uniqid = -1;
	pnode->nodeid = -1;
	pnode->sensid = -1;
	pnode->appsid = -1;
	pnode->timestring[0] = 0x0;
	/** set these here? never changes? */
	pnode->ptiny = (tinyos_header*) &pnode->packet[XMESH_TINY_OFFSET];
	pnode->pmesh = (xmesh_header*) &pnode->packet[XMESH_MESH_OFFSET];
	pnode->psens = (xsense_header*) &pnode->packet[XMESH_SENS_OFFSET];
	pnode->pdata = (xword*) &pnode->packet[XMESH_DATA_OFFSET];
	list_setup(&pnode->data,LIST_TYPE_DEFAULT);
}
/*----------------------------------------------------------------------------*/
void free_wsdata(void* pwsdata)
{
	free(pwsdata); /* data ALWAYS on heap! */
}
/*----------------------------------------------------------------------------*/
void clean_wsnode(wsnode* pnode)
{
	list_clean(&pnode->data,&free_wsdata);
}
/*----------------------------------------------------------------------------*/
void free_wsnode(void* pnode)
{
	clean_wsnode((wsnode*)pnode);
	free(pnode);
}
/*----------------------------------------------------------------------------*/
void clone_wsnode(wsnode* pnode,wsnode* pinit)
{
	setup_wsnode(pnode);
	pnode->uniqid = pinit->uniqid;
	pnode->nodeid = pinit->nodeid;
	pnode->sensid = pinit->sensid;
	pnode->appsid = pinit->appsid;
	pnode->dcount = pinit->dcount; /* need this to verify board */
	/* skip copying data/subs list */
}
/*----------------------------------------------------------------------------*/
void txdat_wsnode(wsnode* pnode,wsnode* pinit)
{
	pnode->countp = pinit->countp;
	memcpy((void*)pnode->packet,(void*)pinit->packet,pinit->countp);
	pnode->dcount = pinit->dcount;
	strcpy(pnode->timestring,pinit->timestring);
	pnode->timestamp = pinit->timestamp;
}
/*----------------------------------------------------------------------------*/
void frame2wsnode(wsnode* pnode,tinyos_sframe* pframe)
{
	pnode->uniqid = -1;
	pnode->nodeid = -1;
	pnode->sensid = -1;
	pnode->appsid = -1;
	pnode->countp = pframe->countp;
	memcpy((void*)pnode->packet,(void*)pframe->packet,pframe->countp);
	/* pre-calculate data count */
	if(pframe->countp>XMESH_DATA_OFFSET)
	{
		pnode->dcount = (pframe->countp-XMESH_DATA_OFFSET)/sizeof(xword);
		pnode->pdata = (xword*) &pnode->packet[XMESH_DATA_OFFSET];
	}
	else
	{
		pnode->dcount = pframe->ptiny->length/sizeof(xword);
		pnode->pdata = (xword*) &pnode->packet[XMESH_BASE_OFFSET];
	}
	/* create string version for time */
	sprintf(pnode->timestring,DATETIME_FORMAT,
		1900 + pframe->timestamp.tm_year,(pframe->timestamp.tm_mon+1),
		pframe->timestamp.tm_mday,pframe->timestamp.tm_hour,
		pframe->timestamp.tm_min,pframe->timestamp.tm_sec);
	pnode->timestamp = pframe->timestamp;
}
/*----------------------------------------------------------------------------*/
/** HACK! */
typedef struct __my1range
{
	float min, max;
}
my1range;
#define LEVEL_COUNT 4
static my1range index_level[LEVEL_COUNT]=
	{{100.0,75.0},{75.0,50.0},{50.0,25.0},{25.0,0.0}};
static my1range range_CO_2[LEVEL_COUNT]=
	{{350.0,600.0},{600.0,1000.0},{1000.0,1500.0},{1500.0,5000.0}};
static my1range range_DUST[LEVEL_COUNT]=
	{{0.0,0.02},{0.02,0.15},{0.15,0.18},{0.18,0.60}};
static my1range range_TEMP[LEVEL_COUNT]=
	{{0.0,2.0},{2.0,5.0},{5.0,10.0},{10.0,24.0}};
static my1range range_HUMI[LEVEL_COUNT]=
	{{0.0,16.0},{16.0,26.0},{26.0,36.0},{36.0,55.0}};
/*----------------------------------------------------------------------------*/
float calc_index(my1range* pindex, my1range* prange, float value, float* pabs)
{
	int level = -1, loop;
	float check = value;
	/* in case need abs */
	if(pabs)
	{
		check -= *pabs;
		if(check<0.0) check = -check;
	}
	/* check boundary */
	if(check<=prange[0].min)
		check = ((prange[0].max-prange[0].min)/2)+prange[0].min;
	else if(check>prange[LEVEL_COUNT-1].max)
		check = prange[0].max;
	/* check level */
	for(loop=0;loop<LEVEL_COUNT;loop++)
	{
		if(check>prange[loop].min&&check<=prange[loop].max)
		{
			level = loop;
			break;
		}
	}
	if(level<0) return 0.0; /** SHOULD NOT BE THE CASE! */
	/* calculate! */
	check = check-prange[loop].min;
	check = check*(pindex[loop].min-pindex[loop].max);
	check = check/(prange[loop].max-prange[loop].min);
	check = pindex[loop].min-check;
	return check;
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int calc_node_data(wsnode* pnode)
{
	int index = 0, error = 0;
	/** HACK! */
	int count_index = 0;
	float value_index = 0.0;
	if(pnode->data.count==pnode->dcount)
	{
		pnode->data.curr = 0x0;
		while(list_iterate(&pnode->data))
		{
			wsdata* pdata = (wsdata*) pnode->data.curr->item;
			sbdata* psens = pdata->psensdata;
			pdata->rawvalue = pnode->pdata[index];
			pdata->voltvalue = 0.0;
			pdata->voltvalid = 0;
			pdata->realvalue = 0.0;
			pdata->realvalid = 0;
			/** HACK! */
			pdata->idxvalue = 0.0;
			pdata->idxvalid = 0;
			/* calculate voltage value - if board info provided */
			if(psens)
			{
				my1item* pvalue = psens->params.init;
				/* calculate adc value */
				pdata->voltvalue = ((float)pdata->rawvalue/
					psens->adc_max)*psens->adc_vrf;
				pdata->voltvalid = 1;
				/* calculate actual value - if equation provided */
				if(psens->pequ&&pvalue)
				{
					my1value* pval = (my1value*) pvalue->item;
					value4f(pval,pdata->voltvalue);
					eval_expression(psens->pequ);
					if(psens->pequ->flag==EXPR_FLAG_SUCCESS)
					{
						pdata->realvalue = value2f(&psens->pequ->value);
						pdata->realvalid = 1;
					}
					else error++;
				}
				/* keep calculated value in param list */
				pvalue = pvalue->next;
				if(pvalue)
				{
					my1value* pval = (my1value*) pvalue->item;
					value4f(pval,pdata->realvalue);
				}
			}
			index++; /** need this here to index pnode->pdata[] */
			/** HACK! */
			if(pdata->realvalid)
			{
				if(!strcmp(psens->label,"CO2"))
				{
					pdata->idxvalid = 1;
					pdata->idxvalue = calc_index(index_level,range_CO_2,
						pdata->realvalue,0x0);
					count_index++;
					value_index += pdata->idxvalue;
				}
				else if(!strcmp(psens->label,"Dust Density"))
				{
					pdata->idxvalid = 1;
					pdata->idxvalue = calc_index(index_level,range_DUST,
						pdata->realvalue,0x0);
					count_index++;
					value_index += pdata->idxvalue;
				}
				else if(!strcmp(psens->label,"Temperature"))
				{
					float abs = 24.0;
					pdata->idxvalid = 1;
					pdata->idxvalue = calc_index(index_level,range_TEMP,
						pdata->realvalue,&abs);
					count_index++;
					value_index += pdata->idxvalue;
				}
				else if(!strcmp(psens->label,"Humidity"))
				{
					float abs = 55.0;
					pdata->idxvalid = 1;
					pdata->idxvalue = calc_index(index_level,range_HUMI,
						pdata->realvalue,&abs);
					count_index++;
					value_index += pdata->idxvalue;
				}
			}
		}
		/** HACK! */
		pnode->idxvalue = 0.0;
		pnode->idxvalid = 0;
		if(count_index==4)
		{
			pnode->idxvalue = value_index/count_index;
			pnode->idxvalid = 1;
		}
	}
	return error ? 0 : index;
}
/*----------------------------------------------------------------------------*/
int execute_query(sqlite3* pdbase, char* pquery)
{
	int result = QUERY_OK;
	sqlite3_stmt *pstate;
	if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK)
	{
#ifdef MY1DEBUG
		printf("Cannot prepare '%s'\n\n",pquery);
#endif
		result = QUERY_ERROR_PREPARE;
	}
	else
	{
		if(sqlite3_step(pstate)!=SQLITE_DONE)
		{
#ifdef MY1DEBUG
			printf("Cannot execute '%s'\n\n",pquery);
#endif
			result = QUERY_ERROR_EXECUTE;
		}
	}
	sqlite3_finalize(pstate);
	return result;
}
/*----------------------------------------------------------------------------*/
int create_rawdata(sqlite3* pdbase, char* pquery, wsnode* pnode)
{
	int result = QUERY_OK;
	sqlite3_stmt *pstate;
	if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK)
	{
		result = QUERY_ERROR_PREPARE;
	}
	else
	{
		char* pText = (char*) pnode->timestring;
		int cSizeT = DATETIME_FORMAT_LEN-1;
		void* pPack = (void*) pnode->packet;
		int cSizeP = pnode->countp;
		do
		{
			if(sqlite3_bind_int(pstate,1,pnode->uniqid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_text(pstate,2,(const char*)pText,
				cSizeT,SQLITE_TRANSIENT)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_blob(pstate,3,(const void*)pPack,
				cSizeP,SQLITE_TRANSIENT)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_step(pstate)!=SQLITE_DONE)
				result = QUERY_ERROR_EXECUTE;
		}
		while(0);
	}
	sqlite3_finalize(pstate);
	return result;
}
/*----------------------------------------------------------------------------*/
int create_prodata(sqlite3* pdbase, char* pquery, wsnode* pnode, int index)
{
	int result = QUERY_OK;
	sqlite3_stmt *pstate;
	if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK)
	{
		result = QUERY_ERROR_PREPARE;
	}
	else
	{
		my1item* pitem = list_select(&pnode->data,index);
		wsdata* pdata = (wsdata*) pitem->item;
		do
		{
			char* pText = (char*) pnode->timestring;
			int cSizeT = DATETIME_FORMAT_LEN-1;
			if(sqlite3_bind_int(pstate,1,pdata->psensdata->uniqid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,2,pdata->rawvalue)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_double(pstate,3,pdata->voltvalue)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,4,pdata->voltvalid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_double(pstate,5,pdata->realvalue)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,6,pdata->realvalid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,7,pnode->nodeid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,8,pnode->sensid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,9,pnode->appsid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_text(pstate,10,(const char*)pText,cSizeT,
					SQLITE_TRANSIENT)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_step(pstate)!=SQLITE_DONE)
				result = QUERY_ERROR_EXECUTE;
		}
		while(0);
	}
	sqlite3_finalize(pstate);
	return result;
}
/*----------------------------------------------------------------------------*/
int setup_database(sqlite3* pdbase)
{
	int result = QUERY_OK;
	{
		/** create table for sensor node */
		char pquery[] =
			"CREATE TABLE IF NOT EXISTS my1sens ( "
			"UniqId INTEGER PRIMARY KEY, "
			"SensId INTEGER UNIQUE NOT NULL, "
			"CountD INTEGER );";
		result = execute_query(pdbase,pquery);
		if(result) return result;
	}
	{
		/** create table for sensor data */
		char pquery[] =
			"CREATE TABLE IF NOT EXISTS my1sensdata ( "
			"UniqId INTEGER PRIMARY KEY, "
			"SensId INTEGER NOT NULL, "
			"IndexD INTEGER, "
			"DataType INTEGER, "
			"ADCBit INTEGER, "
			"ADCVrf REAL, "
			"LabelD TEXT, "
			"UnitsL TEXT, "
			"MathEq TEXT, "
			"UNIQUE (SensId,IndexD) , "
			"FOREIGN KEY(SensId) REFERENCES my1sens(SensId) );";
		result = execute_query(pdbase,pquery);
		if(result) return result;
	}
	{
		/** create table for node (unique node-sensor-app combination!) */
		char pquery[] =
			"CREATE TABLE IF NOT EXISTS my1node ( "
			"UniqId INTEGER PRIMARY KEY, "
			"NodeId INTEGER NOT NULL, "
			"SensId INTEGER NOT NULL, "
			"AppsId INTEGER NOT NULL, "
			"UNIQUE (NodeId,SensId,AppsId) );";
		result = execute_query(pdbase,pquery);
		if(result) return result;
	}
	{
		/** create table for raw data */
		char pquery[] =
			"CREATE TABLE IF NOT EXISTS my1dataraw ( "
			"UniqId INTEGER PRIMARY KEY, "
			"UnodId INTEGER, "
			"TimeStr TEXT, "
			"RawData BLOB, "
			"FOREIGN KEY(UnodId) REFERENCES my1node(UniqId) );";
		result = execute_query(pdbase,pquery);
		if(result) return result;
	}
	{
		/** create table for processed data */
		char pquery[] =
			"CREATE TABLE IF NOT EXISTS my1data ( "
			"UniqId INTEGER PRIMARY KEY, "
			"DataId INTEGER NOT NULL, "
			"RawDValue INTEGER, "
			"VoltValue REAL, "
			"VoltValid INTEGER, "
			"RealValue REAL, "
			"RealValid INTEGER, "
			"NodeId INTEGER, "
			"SensId INTEGER, "
			"AppsId INTEGER, "
			"TimeStr TEXT, "
			"FOREIGN KEY(DataId) REFERENCES my1sensdata(UniqId) );";
		result = execute_query(pdbase,pquery);
		if(result) return result;
	}
	return result;
}
/*----------------------------------------------------------------------------*/
int get_uniqid(sqlite3* pdbase,wsnode* pnode)
{
	int result = QUERY_OK;
	sqlite3_stmt *pstate;
	char pquery[] = "SELECT UniqId FROM my1node WHERE "
		"NodeId=?1 and SensId=?2 and AppsId=?3 ;";
	if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK)
	{
		result = QUERY_ERROR_PREPARE;
	}
	else
	{
		int nid = pnode->pmesh->org_addr;
		int bid = pnode->psens->board_id;
		int aid = pnode->pmesh->app_id;
		do
		{
			if(sqlite3_bind_int(pstate,1,nid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,2,bid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,3,aid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_step(pstate)!=SQLITE_ROW)
			{
				result = QUERY_ERROR_EXECUTE;
			}
			else /** record exists */
			{
				pnode->uniqid = sqlite3_column_int(pstate,0);
				pnode->nodeid = nid;
				pnode->sensid = bid;
				pnode->appsid = aid;
			}
		}
		while(0);
		sqlite3_finalize(pstate);
	}
	return result;
}
/*----------------------------------------------------------------------------*/
int set_uniqid(sqlite3* pdbase,wsnode* pnode)
{
	int result = QUERY_OK;
	sqlite3_stmt *pstate;
	char pquery[] = "INSERT INTO my1node (NodeId,SensId,AppsId) "
		"VALUES (?1,?2,?3);";
	if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK)
	{
		result = QUERY_ERROR_PREPARE;
	}
	else
	{
		int nid = pnode->pmesh->org_addr;
		int bid = pnode->psens->board_id;
		int aid = pnode->pmesh->app_id;
		do
		{
			if(sqlite3_bind_int(pstate,1,nid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,2,bid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_bind_int(pstate,3,aid)!=SQLITE_OK)
			{
				result = QUERY_ERROR_BINDVAL; break;
			}
			if(sqlite3_step(pstate)!=SQLITE_DONE)
				result = QUERY_ERROR_EXECUTE;
		}
		while(0);
		sqlite3_finalize(pstate);
	}
	if(result==QUERY_OK) result = get_uniqid(pdbase,pnode);
	return result;
}
/*----------------------------------------------------------------------------*/
int check_database(sqlite3* pdbase,wsnode* pnode)
{
	int result = QUERY_OK, check = 0;
	if(get_uniqid(pdbase,pnode)!=QUERY_OK) /** try to get existing uniqid */
	{
		if(set_uniqid(pdbase,pnode)!=QUERY_OK) /** try to create new uniqid */
			return QUERY_ERROR_CHKNODE;
		check++;
	}
	/** check in data now! */
	char pquery[] = "INSERT INTO my1dataraw "
		"(UnodId,TimeStr,RawData) VALUES (?1,?2,?3);";
	result = create_rawdata(pdbase,pquery,pnode);
	/* returns check as positive if new node created */
	return result ? result : check;
}
/*----------------------------------------------------------------------------*/
int calc2_database(sqlite3* pdbase,wsnode* pnode)
{
	int result = QUERY_OK, count;
	/** should already have a uniqid! */
	if(pnode->uniqid<0)
		return QUERY_ERROR_CHKNODE;
	/** check in processed data */
	for(count=0;count<pnode->dcount;count++)
	{
		char pquery[] = "INSERT INTO my1data "
			"(DataId,RawDValue,VoltValue,VoltValid,RealValue,RealValid,NodeId,"
			"SensId,AppsId,TimeStr) VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,?10);";
		result = create_prodata(pdbase,pquery,pnode,count);
		if(result) break;
	}
	return result;
}
/*----------------------------------------------------------------------------*/
int setup_node_list(my1list* plist,sqlite3* pdbase)
{
	list_setup(plist,LIST_TYPE_DEFAULT);
	sqlite3_stmt *pstate;
	char pquery[] = "SELECT UniqId,NodeId,SensId,AppsId FROM my1node;";
	if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
	{
		while(sqlite3_step(pstate)==SQLITE_ROW)
		{
			wsnode *pnode = (wsnode*) malloc(sizeof(wsnode));
			setup_wsnode(pnode);
			pnode->uniqid = sqlite3_column_int(pstate,0);
			pnode->nodeid = sqlite3_column_int(pstate,1);
			pnode->sensid = sqlite3_column_int(pstate,2);
			pnode->appsid = sqlite3_column_int(pstate,3);
			list_push_item(plist,(void*)pnode);
		}
	}
	sqlite3_finalize(pstate);
	return plist->count;
}
/*----------------------------------------------------------------------------*/
int clone_node_list(my1list *plist, sqlite3* pdbase)
{
	int result = QUERY_OK;
	/** remove all node info */
	{
		sqlite3_stmt *pstate;
		char pquery[] = "DELETE FROM my1node;";
		if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
		{
			if(sqlite3_step(pstate)!=SQLITE_DONE)
				result = QUERY_ERROR_EXECUTE;
		}
		else result = QUERY_ERROR_PREPARE;
		sqlite3_finalize(pstate);
		if(result!=QUERY_OK) return result;
	}
	/** insert all node in list into database */
	plist->curr = 0x0;
	while(list_iterate(plist))
	{
		result = QUERY_OK;
		sqlite3_stmt *pstate;
		wsnode *pnode = (wsnode*) plist->curr->item;
		char pquery[] = "INSERT INTO my1node (UniqId,NodeId,SensId,AppsId) "
			"VALUES (?1,?2,?3,?4);";
		if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
		{
			do
			{
				if(sqlite3_bind_int(pstate,1,pnode->uniqid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,2,pnode->nodeid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,3,pnode->sensid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,4,pnode->appsid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_step(pstate)!=SQLITE_DONE)
					result = QUERY_ERROR_EXECUTE;
			}
			while(0);
		}
		else result = QUERY_ERROR_PREPARE;
		sqlite3_finalize(pstate);
		if(result!=QUERY_OK) break;
	}
	return result;
}
/*----------------------------------------------------------------------------*/
int clean_node_list(my1list* plist)
{
	list_clean(plist,&free_wsnode);
	return plist->count;
}
/*----------------------------------------------------------------------------*/
