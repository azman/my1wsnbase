/*----------------------------------------------------------------------------*/
#include "my1xmesh.h"
#include "my1comlib.h"
#include "my1cons.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#ifndef MY1APP_PROGNAME
#define MY1APP_PROGNAME "my1wsnview"
#endif
#ifndef MY1APP_PROGVERS
#define MY1APP_PROGVERS "build"
#endif
#ifndef MY1APP_PROGINFO
#define MY1APP_PROGINFO "A simple Crossbow IRIS Base interface"
#endif
/*----------------------------------------------------------------------------*/
#define ERROR_GENERAL -1
#define ERROR_NO_PORT -2
/*----------------------------------------------------------------------------*/
#define OPT_SHOWBEAT 0x00000001
#define OPT_SHOWRAWD 0x00000002
/*----------------------------------------------------------------------------*/
typedef unsigned int termopts_t;
/*----------------------------------------------------------------------------*/
void about(void)
{
	printf("Use: %s [options]\n",MY1APP_PROGNAME);
	printf("Options are:\n");
	printf("  --port <number> : port number between 1-%d.\n",MAX_COM_PORT);
	printf("  --tty <device>  : alternate device name (useful in Linux).\n");
	printf("  --help : show this message. overrides other options.\n");
	printf("  --scan : scan and list ports. overrides other options.\n");
	printf("  --rawd : show raw data.\n");
	printf("  --beat : show heartbeat.\n\n");
}
/*----------------------------------------------------------------------------*/
void print_portscan(ASerialPort_t* aPort)
{
	int test, cCount = 0;
	printf("\n--------------------\n");
	printf("COM Port Scan Result\n");
	printf("--------------------\n");
	for(test=1;test<=MAX_COM_PORT;test++)
	{
		if(check_serial(aPort,test))
		{
			printf("%s%d: ",aPort->mPortName,COM_PORT(test));
			cCount++;
			printf("Ready.\n");
		}
	}
	printf("\nDetected Port(s): %d (%s)\n\n",cCount,aPort->mPortName);
}
/*----------------------------------------------------------------------------*/
void print_currtime(FILE* pfile, struct tm *ptimestamp)
{
	struct tm timestamp;
	char timestring[16];
	if(!ptimestamp)
	{
		time_t currtime = time(0x0);
		timestamp = *localtime(&currtime);
		ptimestamp = &timestamp;
	}
	sprintf(timestring,"%04d%02d%02d:%02d%02d%02d",
		1900 + ptimestamp->tm_year,(ptimestamp->tm_mon+1),
		ptimestamp->tm_mday,ptimestamp->tm_hour,
		ptimestamp->tm_min,ptimestamp->tm_sec);
	fprintf(pfile,"[[%s]] ",timestring);
}
/*----------------------------------------------------------------------------*/
void print_frame_raw(FILE* pfile, tinyos_sframe* pframe)
{
	int loop; /** print non-format */
	for(loop=0;loop<pframe->countb;loop++)
		fprintf(pfile,"[%02X]",pframe->buffer[loop]);
	fprintf(pfile,"\n");
}
/*----------------------------------------------------------------------------*/
void print_frame_wsn(FILE* pfile, tinyos_sframe* pframe)
{
	xword *pdata = (xword*) &pframe->packet[pframe->tag];
	/** tinyos header */
	fprintf(pfile,"[TINYOS] => ");
	fprintf(pfile,"DestAddr: %04X, Group: %02X, Length: %02X",
		pframe->ptiny->dst_addr,pframe->ptiny->am_group,pframe->ptiny->length);
	if(pframe->ptiny->length>1&&pframe->ptiny->am_type==AM_HEARTBEAT)
		fprintf(pfile,", Sequence: %04X",*pdata);
	/** xmesh header */
	if(pframe->countp>XMESH_SENS_OFFSET)
	{
		fprintf(pfile,"\n\t[XMESH] => Origin: %04X, Source: %04X, ",
			pframe->pmesh->org_addr,pframe->pmesh->src_addr);
		fprintf(pfile,"Sequence: %04X, AppID: %02X",
			pframe->pmesh->seq_num, pframe->pmesh->app_id);
	}
	/** xsense header */
	if(pframe->countp>XMESH_DATA_OFFSET)
	{
		int loop = pframe->tag, count = 0;
		if(pframe->ptiny->am_type==AM_DATA2BASE)
		{
			fprintf(pfile,"\n\t[BOARD] => BoardID: %02X, PacketID: %02X",
				pframe->psens->board_id,pframe->psens->packet_id);
			fprintf(pfile,"\n\t[DATA] => ");
			while(loop<pframe->countp)
			{
				fprintf(pfile,"[%04X] ",*pdata);
				loop += sizeof(xword); count++; pdata++;
			}
			fprintf(pfile,"{%d}",count);
		}
		else if(pframe->ptiny->am_type==AM_HEALTH)
		{
			xmesh_health_header *phealth = 
				(xmesh_health_header*) &pframe->packet[XMESH_SENS_OFFSET];
			int is_mesh2 = (phealth->type_node&0xF0)==0xF0 ? 1 : 0;
			fprintf(pfile,"\n\t[HEALTH] => Node Type: %s (%02X), ",
				is_mesh2?"XMesh2":"XMesh",phealth->type_node&0xF0);
			fprintf(pfile,"NCount: %d (%02X), ",
				phealth->type_node&0x0F,phealth->type_node&0x0F);
			fprintf(pfile,"Version: %02X, Type: %s (%02X)",
				phealth->version,phealth->type==0x01?"Statistic":
				phealth->type==0x02?"NEIGHBORHOOD":"UNKNOWN",phealth->type);
			switch(phealth->type)
			{
				case 0x01:
				{
					xmesh_health_stat *pstat = 
						(xmesh_health_stat*) &pframe->packet[XMESH_SENS_OFFSET];
					fprintf(pfile,"\n\t[STAT] => Battery: %d (%02X), ",
						pstat->battery_voltage,pstat->battery_voltage);
					fprintf(pfile,"SeqNum: %04X",pstat->seq_num);
					fprintf(pfile,"\n\t[QUALITY] => Node: %d (%02X), ",
						pstat->linkinfo[0].node_id,pstat->linkinfo[0].node_id);
					fprintf(pfile,"Link Quality: %02X, Path Cost: %02X, ",
						pstat->linkinfo[0].link_quality,
						pstat->linkinfo[0].hop_count);
					fprintf(pfile,"Link Indicator: %02X",
						pstat->linkinfo[0].radio_link_indicator);
					break;
				}
				case 0x02:
				{
					xmesh_health_hood *phood =
						(xmesh_health_hood*) &pframe->packet[XMESH_SENS_OFFSET];
					fprintf(pfile,"\n\t[QUALITY] => Node: %d (%02X), ",
						phood->linkinfo[0].node_id,phood->linkinfo[0].node_id);
					fprintf(pfile,"Link Quality: %02X, Path Cost: %02X, ",
						phood->linkinfo[0].link_quality,
						phood->linkinfo[0].hop_count);
					fprintf(pfile,"Link Indicator: %02X",
						phood->linkinfo[0].radio_link_indicator);
					break;
				}
			}
		}
		else
		{
			fprintf(pfile,"\n\t[DATA] => ");
			while(loop<pframe->countp)
			{
				fprintf(pfile,"[%04X] ",pframe->packet[loop++]);
				count++;
			}
			fprintf(pfile,"{%d}",count);
		}
	}
	fprintf(pfile,"\n");
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	ASerialPort_t cPort;
	ASerialConf_t cConf;
	my1key_t key;
	termopts_t options = 0x0;
	int terminal = 1, test, loop, paused = 0;
	int help = 0, scan = 0;
	char *ptty = 0x0;
	/** xmesh structure */
	tinyos_sframe tosframe;

	/* print info */
	printf("\n%s - %s (%s)\n",MY1APP_PROGNAME,MY1APP_PROGINFO,MY1APP_PROGVERS);
	printf("  => by azman@my1matrix.net\n\n");

	/* check program arguments */
	if(argc>1)
	{
		for(loop=1;loop<argc;loop++)
		{
			if(argv[loop][0]=='-') /* options! */
			{
				if(!strcmp(argv[loop],"--help"))
				{
					help = 1;
				}
				else if(!strcmp(argv[loop],"--port"))
				{
					if(get_param_int(argc,argv,&loop,&test)<0)
					{
						printf("Cannot get port number! Using default (%d)!\n",
							terminal);
					}
					else if(test>MAX_COM_PORT)
					{
						printf("Invalid port number %d! Using default (%d)!\n",
							test,terminal);
					}
					else terminal = test;
				}
				else if(!strcmp(argv[loop],"--tty"))
				{
					if(!(ptty=get_param_str(argc,argv,&loop)))
					{
						printf("Error getting tty name!\n");
						continue;
					}
				}
				else if(!strcmp(argv[loop],"--scan"))
				{
					scan = 1;
				}
				else if(!strcmp(argv[loop],"--raw"))
				{
					options |= OPT_SHOWRAWD; /* display raw data */
				}
				else if(!strcmp(argv[loop],"--beat"))
				{
					options |= OPT_SHOWBEAT; /* display heart beat */
				}
				else
				{
					printf("Unknown option '%s'!\n",argv[loop]);
				}
			}
			else /* not an option? */
			{
				printf("Unknown parameter %s!\n",argv[loop]);
				return ERROR_GENERAL;
			}
		}
	}

	/* check if user requested help */
	if(help)
	{
		about();
		return 0;
	}

	/* initialize port */
	initialize_serial(&cPort);
#ifndef DO_MINGW
	sprintf(cPort.mPortName,"/dev/ttyUSB"); /* default on linux? */
#endif
	/* check user requested to change port name */
	if(ptty) sprintf(cPort.mPortName,"%s",ptty);

	/* check if user requested a port scan */
	if(scan)
	{
		print_portscan(&cPort);
		return 0;
	}

	/* try to prepare port with requested terminal */
	if(!terminal) terminal = find_serial(&cPort,0x0);
	if(!set_serial(&cPort,terminal))
	{
		printf("Cannot prepare port '%s%d'!\n\n",
			cPort.mPortName,COM_PORT(terminal));
		return ERROR_NO_PORT;
	}

	/* apply baudrate for crossbow mote */
	get_serialconfig(&cPort,&cConf);
	cConf.mBaudRate = MY1BAUD57600;
	set_serialconfig(&cPort,&cConf);

	/* try opening port */
	if(!open_serial(&cPort))
	{
		printf("Cannot open port '%s%d'!\n\n",cPort.mPortName,
			COM_PORT(cPort.mPortIndex));
		return ERROR_GENERAL;
	}

	/* initialize tinyos buffer */
	tosframe.indexb = 0;

	/* clear input buffer */
	purge_serial(&cPort);

	/* start main loop */
	while(1)
	{
		key = get_keyhit();
		if(key!=KEY_NONE)
		{
			if(key==KEY_F1) /* show commands */
			{
				printf("\n\n------------\n");
				printf("Command Keys\n");
				printf("------------\n");
				printf("<F1>  - Show this help\n");
				printf("<F2>  - Show Current Settings\n");
				printf("<F3>  - Toggle heartbeat display flag\n");
				printf("<F4>  - Toggle raw data display flag\n");
				printf("<SPC> - Pause/Start Reading\n");
				printf("<ESC> - Exit this program\n");
			}
			else if(key==KEY_F2) /* show current settings */
			{
				printf("\n\nCurrent Settings:\n");
				printf("\nConnected to '%s%d'!",cPort.mPortName,
					COM_PORT(cPort.mPortIndex));
				if(options&OPT_SHOWBEAT) printf("\nShowing heartbeat!");
				else printf("\nHidden heartbeat.");
				if(options&OPT_SHOWRAWD) printf("\nShowing raw data!");
				else printf("\nHidden raw data.");
				printf("\n\n");
			}
			else if(key==KEY_F3) /* toggle heartbeat display */
			{
				options ^= OPT_SHOWBEAT;
				if(options&OPT_SHOWBEAT) printf("\n\nHeartBeat ON!\n\n");
				else printf("\n\nHeartBeat OFF!\n\n");
			}
			else if(key==KEY_F4) /* toggle raw data display */
			{
				options ^= OPT_SHOWRAWD;
				if(options&OPT_SHOWRAWD) printf("\n\nDisplay RAW Data!\n\n");
				else printf("\n\nHidden RAW Data!\n\n");
			}
			else if(key==KEY_SPACE) /* toggle start/pause reading */
			{
				paused = !paused;
				if(paused){  printf("\n[PAUSED]\n"); tosframe.indexb = 0; }
				else printf("\n[RUNNING]\n");
			}
			else if(key==KEY_ESCAPE)
			{
				printf("\n\nUser Program '%s' Exit.\n\n",MY1APP_PROGNAME);
				break;
			}
		}
		if(paused) continue;
		/* check serial port for incoming data */
		if(check_incoming(&cPort))
		{
			byte_t cTemp = get_byte_serial(&cPort);
			switch(check_inbyte(&tosframe,cTemp))
			{
				case XMESHF_CHECK_OVERFLOW:
				{
					print_currtime(stderr,0x0);
					fprintf(stderr,"[WARN] Serial Buffer Overflow {MAX:%d}\n",
						XMESHF_PACKET_MAXBYTE);
					break;
				}
				case XMESHF_CHECK_COMPLETE:
				{
					if(!(options&OPT_SHOWBEAT)&&
							tosframe.ptiny->am_type==AM_HEARTBEAT)
						break;
					print_currtime(stdout,&tosframe.timestamp);
					if(tosframe.ptiny->am_type==AM_DATA2BASE)
						fprintf(stdout,"[AM_DATA2BASE] ");
					else if(tosframe.ptiny->am_type==AM_HEARTBEAT)
						fprintf(stdout,"[AM_HEARTBEAT] ");
					else if(tosframe.ptiny->am_type==AM_HEALTH)
						fprintf(stdout,"[AM_HEALTH] ");
					else
						fprintf(stdout,"[AM_TYPE=%02X] ",
							tosframe.ptiny->am_type);
					if(options&OPT_SHOWRAWD)
						print_frame_raw(stdout,&tosframe);
					print_frame_wsn(stdout,&tosframe);
					break;
				}
				case XMESHF_CHECK_CONTINUE:
				{
					/** do nothing! */
					break;
				}
				default:
				{
					/** frame errors! */
					print_currtime(stderr,0x0);
					fprintf(stderr,"[ERROR] Invalid frame (%d)\n",tosframe.tag);
				}
			}
		}
	}
	/* close port */
	close_serial(&cPort);
	return 0;
}
/*----------------------------------------------------------------------------*/
