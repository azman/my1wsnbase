/*----------------------------------------------------------------------------*/
#ifndef __MY1XMESHH__
#define __MY1XMESHH__
/*----------------------------------------------------------------------------*/
#include <time.h> /* just because of struct tm! */
/*----------------------------------------------------------------------------*/
/**
 * Standard Active Message packet types used by XMesh are defined here.
 *
 * User AM types are 100 to 200 (0x64 to 0xC8)
 *
 * AM_DEBUGPACKET     : Health packet
 * AM_DATA2BASE       : Upstream data message to base
 * AM_DATA2NODE       : Downstream data to node
 * AM_DATAACK2BASE    : Upstream data to base requiring an ack
 * AM_DATAACCK2NODE   : Downstream data to node requiring an ack
 * AM_DOWNSTREAM_ACK  : Acknowledge message from base to the originating mote
 * AM_UPSTREAM_ACK    : Acknowledge message from node to the originating base
 * AM_PATH_LIGHT DOWN : Create dedicated stream to node for designated time
 * AM_PATH_LIGHT_UP   : Create dedicated stream from node for designated time
 * AM_MULTIHOPMSG     : Route Update message
 */
#define AM_HEALTH          0x03
#define AM_DEBUGPACKET     0x03
#define AM_DATA2BASE       0x0B
#define AM_DATA2NODE       0x0C
#define AM_DATAACK2BASE    0x0D
#define AM_DATAACK2NODE    0x0E
#define AM_ANY2ANY         0x0F
#define AM_TIMESYNC        0xEF
#define AM_PREAMBLE        0xFO
#define AM_DOWNSTREAM_ACK  0xF6
#define AM_UPSTREAM_ACK    0xF7
#define AM_PATH_LIGHT_DOWN 0xF8
#define AM_PATH_LIGHT_UP   0xF9
#define AM_MULTIHOPMSG     0xFA
#define AM_ONE_HOP         0xFB
#define AM_HEARTBEAT       0xFD
/* OTAP-related */
#define AM_MGMT            0x5A
#define AM_BULKXFER        0x5B
#define AM_MGMTRESP        0x5C
/*----------------------------------------------------------------------------*/
typedef unsigned char xbyte;
typedef unsigned short xword;
/*----------------------------------------------------------------------------*/
#define XMESHF_BYTE_SYNC 0x7E
#define XMESHF_BYTE_ESCP 0x7D
#define XMESHF_PACKET_MAXBYTE 256
/*----------------------------------------------------------------------------*/
typedef struct __tinyos_header
{
	xword dst_addr;
	xbyte am_type; /* am = active message */
	xbyte am_group;
	xbyte length;
}
tinyos_header;
/*----------------------------------------------------------------------------*/
typedef struct __tinyos_packet_unused
{
	tinyos_header tiny_hdr; /* padded here! */
	xbyte databyte; /* cannot use this address!! */
}
tinyos_packet_unused;
/*----------------------------------------------------------------------------*/
typedef struct __tinyos_packet
{
	xword dst_addr; /* 0x007e is the serial port address */
	xbyte am_type;
	xbyte am_group;
	xbyte length;
	xbyte pdata; /* maybe can use this? */
	/* TOSH_DATA_LENGTH default 29 bytes! */
}
tinyos_packet;
/*----------------------------------------------------------------------------*/
typedef struct __xmesh_header
{
	xword src_addr;
	xword org_addr;
	xword seq_num;
	xbyte app_id;
}
xmesh_header;
/*----------------------------------------------------------------------------*/
typedef struct __xsense_header
{
	xbyte board_id;
	xbyte packet_id;
	xword parent;
}
xsense_header;
/*----------------------------------------------------------------------------*/
typedef struct __health_node_quality
{
	xword node_id; /** parent id */
	xbyte link_quality; /** 0xf0 for tx, 0x0f for rx */
	xbyte hop_count; /** parent to base station hop count */
	xbyte radio_link_indicator; /** rssi/lqi value? */
}
__attribute__ ((packed)) health_node_quality;
/*----------------------------------------------------------------------------*/
typedef struct __xmesh_health_header
{
	xbyte type_node; /** 0xFX for XMesh2 */
	xbyte version; /** 0x20 for XMesh2 */
	xbyte type;  /** 0x02 : neighbor, 0x01 : statistic */
	/** neighbor type => type_node lower nibble for hood count! */
}
__attribute__ ((packed)) xmesh_health_header;
/*----------------------------------------------------------------------------*/
#define HEALTH_NODE_MAX_NODE 2
/*----------------------------------------------------------------------------*/
typedef struct __xmesh_health_stat
{
	xmesh_health_header header;
	/** statistic */
	xword seq_num;  /** sequence number (counter) */
	xword num_node_pkts; /** local generated app packets */
	xword num_fwd_pkts; /** forward count */
	xword num_drop_pkts; /** dropped count */
	xword num_rexmits; /** retransmit count */
	xbyte battery_voltage;
	xword power_sum; /** not used? */
	xbyte rsvd_app_type; /** reserved crossbow field */
	/* actually health info */
	health_node_quality linkinfo[HEALTH_NODE_MAX_NODE];
}
__attribute__ ((packed)) xmesh_health_stat;
/*----------------------------------------------------------------------------*/
#define HEALTH_NODE_MAX_NEIGHBOR 3
/*----------------------------------------------------------------------------*/
typedef struct __xmesh_health_hood
{
	xmesh_health_header header;
	/** neighborhood */
	/* actually health info */
	health_node_quality linkinfo[HEALTH_NODE_MAX_NEIGHBOR];
}
__attribute__ ((packed)) xmesh_health_hood;
/*----------------------------------------------------------------------------*/
#define XMESH_TINYOSH_SIZE 5
#define XMESH_XMESH_SIZE 7
#define XMESH_XSERVEH_SIZE 4
#define XMESH_TINY_OFFSET 0
#define XMESH_BASE_OFFSET 5
#define XMESH_MESH_OFFSET 5
#define XMESH_SENS_OFFSET 12
#define XMESH_DATA_OFFSET 16
#define XMESH_HLTHHEAD_SIZE (3+XMESH_XMESH_SIZE)
#define XMESH_HOODPACK_SIZE (18+XMESH_XMESH_SIZE)
#define XMESH_STATPACK_SIZE (22+XMESH_XMESH_SIZE)
/*----------------------------------------------------------------------------*/
#define XSENSOR_MTS101_BID 0x82
#define XSENSOR_MTS300_BID 0x83
#define XSENSOR_MTS310_BID 0x84
#define XSENSOR_MTS400_BID 0x85
#define XSENSOR_MTS420_BID 0x86
#define XSENSOR_MDA300_BID 0x81
#define XSENSOR_MDA500_BID 0x01
#define XSENSOR_MTS510_BID 0x02
#define XSENSOR_MEP510_BID 0x04
#define XSENSOR_MEP410_BID 0x8A
#define XSENSOR_MDA320_BID 0x90
#define XSENSOR_MDA100_BID 0x91
#define XSENSOR_MSP410_BID 0xA0
#define XSENSOR_MAN100_BID 0x99 /** CUSTOM BOARD BY ARMS - TESTING BOARDS */
#define XSENSOR_MAN101_BID 0xC8 /** CUSTOM BOARD BY ARMS - AIR QUALITY */
#define XSENSOR_MAN102_BID 0xC9 /** CUSTOM BOARD BY ARMS - AIR QUALITY */
#define XSENSOR_MANS01_BID 0xB0 /** CUSTOM BOARD BY ARMS - TERMO & PH */
/*----------------------------------------------------------------------------*/
typedef struct __tinyos_sframe
{
	int tag, countp, countb, indexb;
	xbyte buffer[XMESHF_PACKET_MAXBYTE];
	xbyte packet[XMESHF_PACKET_MAXBYTE]; 
	tinyos_header *ptiny;
	xmesh_header *pmesh;
	xsense_header *psens;
	struct tm timestamp;
}
tinyos_sframe;
/*----------------------------------------------------------------------------*/
#define XMESHF_ERROR_SIZE_H -2
#define XMESHF_ERROR_SIZE_P -3
#define XMESHF_ERROR_NOSYNC -4
#define XMESHF_ERROR_NOTYPE -5
#define XMESHF_ERROR_CRCCHK -6
/*----------------------------------------------------------------------------*/
#define XMESHF_CHECK_OVERFLOW -1
#define XMESHF_CHECK_CONTINUE 0
#define XMESHF_CHECK_COMPLETE 1
/*----------------------------------------------------------------------------*/
xword xcrc_byte(xword xcrc, xbyte data);
int check_inbyte(tinyos_sframe* pframe, xbyte ibyte);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
