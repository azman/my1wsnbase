/*----------------------------------------------------------------------------*/
#ifndef __MY1DBASEH__
#define __MY1DBASEH__
/*----------------------------------------------------------------------------*/
#include "sqlite3.h"
#include "my1xmesh.h"
#include "my1list.h"
#include "my1expr.h"
/*----------------------------------------------------------------------------*/
#define UNITS_MAX_CHAR 16
#define LABEL_MAX_CHAR 32
#define CONVERT_MAX_CHAR 256
/** ISO8601 date time format => YYYY-MM-DDThh:mm:ss.sTZD */
#define DATETIME_FORMAT "%04d-%02d-%02dT%02d:%02d:%02d"
#define DATETIME_FORMAT_LEN (19+1)
/*----------------------------------------------------------------------------*/
#define QUERY_OK 0
#define QUERY_ERROR_PREPARE -1
#define QUERY_ERROR_EXECUTE -2
#define QUERY_ERROR_BINDVAL -3
#define QUERY_ERROR_GOQUERY -4
#define QUERY_ERROR_CHKNODE -5
/*----------------------------------------------------------------------------*/
#define BOARD_OK 0
#define BOARD_ERROR -1
#define BOARD_ELOAD -2
#define BOARD_EDATA -3
#define BOARD_EINFO -4
#define BOARD_EINFO_EXTRA -5
#define BOARD_EMISS -6
#define BOARD_EDATA_MISS -7
#define BOARD_EDATA_EXTRA -8
#define BOARD_EDATA_NONE -9
#define BOARD_EDVAL_MISS -10
#define BOARD_EFORMAT -11
#define BOARD_ERROR_IDXC -12
#define BOARD_EIDXC_MISS -13
/*----------------------------------------------------------------------------*/
typedef struct __sboard
{
	int boardid, datacount;
	my1list sensdata;
}
sboard;
/*----------------------------------------------------------------------------*/
typedef struct __sbdata
{
	int uniqid, datatype; /** datatype = # of decimal points! */
	int adc_bit, adc_max;
	float adc_vrf;
	char label[LABEL_MAX_CHAR], units[UNITS_MAX_CHAR];
	char convert[CONVERT_MAX_CHAR];
	my1expr *pequ;
	my1list params;
}
sbdata;
/*----------------------------------------------------------------------------*/
typedef struct __wsdata
{
	sbdata* psensdata;
	int rawvalue, voltvalid, realvalid;
	float voltvalue, realvalue;
	/** HACK! */
	int idxvalid;
	float idxvalue;
}
wsdata;
/*----------------------------------------------------------------------------*/
typedef struct __wsnode
{
	int uniqid, nodeid, sensid, appsid, countp, dcount;
	xbyte packet[XMESHF_PACKET_MAXBYTE];
	tinyos_header *ptiny;
	xmesh_header *pmesh;
	xsense_header *psens;
	xword *pdata;
	char timestring[DATETIME_FORMAT_LEN];
	struct tm timestamp;
	my1list data;
	/** HACK! */
	int idxvalid;
	float idxvalue;
}
wsnode;
/*----------------------------------------------------------------------------*/
int fopen_sens_list(my1list *plist, char *pdbtext);
int setup_sens_list(my1list* plist, sqlite3* pdbase);
int cp2db_sens_list(my1list* plist, sqlite3* pdbase);
int clean_sens_list(my1list* plist);
sboard* check_sens_list(my1list* plist, int boardid);
/*----------------------------------------------------------------------------*/
void setup_wsnode(wsnode* pnode);
void clean_wsnode(wsnode* pnode);
void clone_wsnode(wsnode* pnode,wsnode* pinit);
void txdat_wsnode(wsnode* pnode,wsnode* pinit);
void frame2wsnode(wsnode* pnode,tinyos_sframe* pframe);
int calc_node_data(wsnode* pnode);
/*----------------------------------------------------------------------------*/
int setup_database(sqlite3* pdbase);
int check_database(sqlite3* pdbase,wsnode* pnode);
int calc2_database(sqlite3* pdbase,wsnode* pnode);
int node4_database(sqlite3* pdbase,wsnode* pnode);
int setup_node_list(my1list *plist, sqlite3* pdbase);
int clone_node_list(my1list *plist, sqlite3* pdbase);
int clean_node_list(my1list* plist);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
