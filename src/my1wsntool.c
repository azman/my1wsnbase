/*----------------------------------------------------------------------------*/
#include "my1list.h"
#include "my1dbase.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#ifndef MY1APP_PROGNAME
#define MY1APP_PROGNAME "my1wsntool"
#endif
#ifndef MY1APP_PROGVERS
#define MY1APP_PROGVERS "build"
#endif
#ifndef MY1APP_PROGINFO
#define MY1APP_PROGINFO "CEASTech WSN Data Tool"
#endif
/*----------------------------------------------------------------------------*/
#define ERROR_PARAM -1
#define ERROR_DBOPEN -2
/*----------------------------------------------------------------------------*/
#define SORTDATA_NAME "basesort"
/*----------------------------------------------------------------------------*/
#define COMMAND_UNKNOWN 0
#define COMMAND_NODELIST 1
#define COMMAND_SENSLIST 2
#define COMMAND_NODECOPY 3
#define COMMAND_FIXRDATA 4
#define COMMAND_LOADSENS 5
#define COMMAND_CALCDATA 6
#define COMMAND_CHKMERGE 7
#define COMMAND_PURGENOD 8
#define COMMAND_PURGEDAT 9
#define COMMAND_KEEPDATA 10
/*----------------------------------------------------------------------------*/
#define WORKBEAT 100
/*----------------------------------------------------------------------------*/
typedef struct _rawdata_t
{
	int rawdid, nodeid;
}
rawdata_t;
/*----------------------------------------------------------------------------*/
void about(void)
{
	printf("Command-line use:\n");
	printf("  %s <command> [options]\n\n",MY1APP_PROGNAME);
	printf("Valid Commands are:\n");
	printf("  nodelist : list nodes\n");
	printf("  senslist : list sensorboards\n");
	printf("  nodecopy : copy nodes (needs --dest param)\n");
	printf("  fixrdata : fix raw data's node uniq id\n");
	printf("  loadsens : load new sensorboard list from text\n");
	printf("  calcdata : recalculate data from raw\n");
	printf("  chkmerge : merge raw data (needs --dest param)\n");
	printf("  purgenod : delete specified node (needs --node param)\n");
	printf("  purgedat : delete all orphan data (not from node in list)\n");
	printf("  keepdata : keep data into path/file database structure\n");
	printf("Options are:\n");
	printf("  --file <dbfile> : DB file name\n");
	printf("  --dest <dbfile> : DB file name (target DB)\n");
	printf("  --sens <dbtext> : sensor text DB file name\n");
	printf("  --node <uniqid> : node UniqId for to be purged\n");
	printf("  --sort   : sort nodes (for node copy/list option)\n");
	printf("  --help   : show this message\n\n");
}
/*----------------------------------------------------------------------------*/
void board_node_list(my1list* pnodelist,my1list* psenslist)
{
	pnodelist->curr = 0x0;
	while(list_iterate(pnodelist))
	{
		wsnode *pnode = (wsnode*) pnodelist->curr->item;
		sboard *pboard = check_sens_list(psenslist,pnode->sensid);
		if(pboard)
		{
			pboard->sensdata.curr = 0x0;
			while(list_iterate(&pboard->sensdata))
			{
				wsdata *pdata = (wsdata*) malloc(sizeof(wsdata));
				pdata->psensdata = (sbdata*) pboard->sensdata.curr->item;
				list_push_item(&pnode->data,(void*)pdata);
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
int clear_data(sqlite3* pdbase)
{
	int done = 0;
	sqlite3_stmt *pstate;
	char pquery[] = "DELETE FROM my1data;";
	if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
	{
		if(sqlite3_step(pstate)==SQLITE_DONE)
		{
			done = 1;
			printf("All processed data deleted!\n");
		}
		else printf("Error executing query '%s'!\n",pquery);
	}
	else printf("Error preparing query '%s'!\n",pquery);
	sqlite3_finalize(pstate);
	return done;
}
/*----------------------------------------------------------------------------*/
int setup_rawd_list(my1list* plist,sqlite3* pdbase)
{
	list_setup(plist,LIST_TYPE_DEFAULT);
	sqlite3_stmt *pstate;
	char pquery[] = "SELECT UniqId FROM my1dataraw;";
	if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
	{
		while(sqlite3_step(pstate)==SQLITE_ROW)
		{
			int *p_uniq = (int*) malloc(sizeof(int));
			*p_uniq = sqlite3_column_int(pstate,0);
			list_push_item(plist,(void*)p_uniq);
		}
	}
	sqlite3_finalize(pstate);
	return plist->count;
}
/*----------------------------------------------------------------------------*/
void free_rawd(void* pnode)
{
	free(pnode);
}
/*----------------------------------------------------------------------------*/
int clean_rawd_list(my1list* plist)
{
	list_clean(plist,&free_rawd);
	return plist->count;
}
/*----------------------------------------------------------------------------*/
int recal_rawd_list(my1list* plist,sqlite3* pdbase,my1list* pnodelist)
{
	int count = 0;
	plist->curr = 0x0;
	while(list_iterate(plist))
	{
		wsnode tnode, *pnode;
		sqlite3_stmt *pstate;
		char pquery[]="SELECT UnodId,TimeStr,RawData FROM my1dataraw "
			"WHERE UniqId=?1;";
		setup_wsnode(&tnode);
		if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
		{
			int *ptest = (int*) plist->curr->item;
			if(sqlite3_bind_int(pstate,1,*ptest)==SQLITE_OK&&
				sqlite3_step(pstate)==SQLITE_ROW)
			{
				char *ptext; void *pblob;
				/* get rawdata */
				tnode.uniqid = sqlite3_column_int(pstate,0);
				ptext = (char*) sqlite3_column_text(pstate,1);
				strcpy(tnode.timestring,ptext);
				pblob = (void*) sqlite3_column_blob(pstate,2);
				tnode.countp = sqlite3_column_bytes(pstate,2);
				memcpy((void*)tnode.packet,pblob,tnode.countp);
				/* pre-calculate data count */
				tnode.dcount = (tnode.countp-XMESH_DATA_OFFSET)/sizeof(xword);
				tnode.pdata = (xword*) &tnode.packet[XMESH_DATA_OFFSET];
			}
		}
		else printf("Error preparing query '%s'!\n",pquery);
		sqlite3_finalize(pstate);
		if(tnode.uniqid>=0)
		{
			pnodelist->curr = 0x0;
			while(list_iterate(pnodelist))
			{
				pnode = (wsnode*) pnodelist->curr->item;
				if(pnode->uniqid==tnode.uniqid)
				{
					txdat_wsnode(pnode,&tnode);
					if(calc_node_data(pnode))
						calc2_database(pdbase,pnode);
					count++;
					if (count%WORKBEAT==0) { putchar('.'); fflush(stdout); }
					break;
				}
			}
		}
		clean_wsnode(&tnode);
	}
	return count;
}
/*----------------------------------------------------------------------------*/
int reset_rawd_list(my1list* plist,sqlite3* pdbase,my1list* pnodelist)
{
	int count = 0, check = QUERY_OK;
	plist->curr = 0x0;
	while(list_iterate(plist))
	{
		sqlite3_stmt *pstate;
		int *ptest = (int*) plist->curr->item;
		void *pblob;
		wsnode tnode;
		setup_wsnode(&tnode);
		/** first, get node info from raw data */
		do
		{
			char pquery[]="SELECT RawData FROM my1dataraw WHERE UniqId=?1;";
			if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
				check=QUERY_ERROR_PREPARE; break; }
			if(sqlite3_bind_int(pstate,1,*ptest)!=SQLITE_OK) {
				check=QUERY_ERROR_BINDVAL; break; }
			if(sqlite3_step(pstate)!=SQLITE_ROW) {
				check=QUERY_ERROR_GOQUERY; break; }
			/* get rawdata */
			pblob = (void*) sqlite3_column_blob(pstate,0);
			tnode.countp = sqlite3_column_bytes(pstate,0);
			memcpy((void*)tnode.packet,pblob,tnode.countp);
		}
		while(0);
		sqlite3_finalize(pstate);
		if(check<0) return check;
		/** update node uniqid for raw data */
		pnodelist->curr = 0x0;
		while(list_iterate(pnodelist))
		{
			wsnode* pnode = (wsnode*) pnodelist->curr->item;
			if(pnode->nodeid==(int)tnode.pmesh->org_addr&&
				pnode->sensid==(int)tnode.psens->board_id&&
				pnode->appsid==(int)tnode.pmesh->app_id)
			{
				tnode.uniqid = pnode->uniqid;
				break;
			}
		}
		if(tnode.uniqid<0) return QUERY_ERROR_CHKNODE;
		/** now we update! */
		do
		{
			char pquery[]="UPDATE my1dataraw SET UnodId=?1 WHERE UniqId=?2;";
			if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
				check=QUERY_ERROR_PREPARE; break; }
			if(sqlite3_bind_int(pstate,1,tnode.uniqid)!=SQLITE_OK) {
				check=QUERY_ERROR_BINDVAL; break; }
			if(sqlite3_bind_int(pstate,2,*ptest)!=SQLITE_OK) {
				check=QUERY_ERROR_BINDVAL; break; }
			if(sqlite3_step(pstate)!=SQLITE_DONE) {
				check=QUERY_ERROR_GOQUERY; break; }
			count++; if (count%WORKBEAT==0) { putchar('.'); fflush(stdout); }
		}
		while(0);
		sqlite3_finalize(pstate);
		if(check<0) return check;
	}
	return count;
}
/*----------------------------------------------------------------------------*/
int print_node(void* pcheck)
{
	wsnode *pnode = (wsnode*) pcheck;
	printf("[Node]=>UniqID:0x%04X,NodeID:0x%02X,SensID:0x%02X,AppsID:0x%02X\n",
		pnode->uniqid,pnode->nodeid,pnode->sensid,pnode->appsid);
	return 0; /* never delete */
}
/*----------------------------------------------------------------------------*/
int print_sens(void* pcheck)
{
	int loop = 0;
	sboard *pboard = (sboard*) pcheck;
	printf("SensID: 0x%02X (%d)\n",pboard->boardid,pboard->datacount);
	pboard->sensdata.curr = 0x0;
	while(list_iterate(&pboard->sensdata))
	{
		sbdata *pdata = (sbdata*) pboard->sensdata.curr->item;
		printf("Data %02d: Bits=%d, Vref=%f, Label='%s', Unit='%s', "
			"Convert='%s'\n",loop,pdata->adc_bit,pdata->adc_vrf,pdata->label,
			pdata->units,pdata->convert);
		loop++;
	}
	return 0; /* never delete */
}
/*----------------------------------------------------------------------------*/
int merge_rawd_base(sqlite3* pdbase, sqlite3* pdbtgt, int *pcount)
{
	int count = 0, error = 0;
	wsnode tnode;
	sqlite3_stmt *pstate;
	char pquery[] = "SELECT TimeStr,RawData FROM my1dataraw;";
	if(sqlite3_prepare_v2(pdbtgt,pquery,-1,&pstate,0)==SQLITE_OK)
	{
		/* iterate through raw data in target db */
		while(sqlite3_step(pstate)==SQLITE_ROW)
		{
			char *ptext; void *pblob;
			/* prepare tnode */
			setup_wsnode(&tnode);
			/* get rawdata */
			ptext = (char*) sqlite3_column_text(pstate,0);
			strcpy(tnode.timestring,ptext);
			pblob = (void*) sqlite3_column_blob(pstate,1);
			tnode.countp = sqlite3_column_bytes(pstate,1);
			memcpy((void*)tnode.packet,pblob,tnode.countp);
			/* pre-calculate data count */
			tnode.dcount = (tnode.countp-XMESH_DATA_OFFSET)/sizeof(xword);
			tnode.pdata = (xword*) &tnode.packet[XMESH_DATA_OFFSET];
			/* try to insert into original db */
			if(check_database(pdbase,&tnode)<0) error++;
			count++; if (count%WORKBEAT==0) { putchar('.'); fflush(stdout); }
			/* clean up */
			clean_wsnode(&tnode);
		}
	}
	sqlite3_finalize(pstate);
	if(pcount) *pcount = count;
	return error;
}
/*----------------------------------------------------------------------------*/
#define MAX_QUERY_SIZE 256
/*----------------------------------------------------------------------------*/
int purge_rawd_base(sqlite3* pdbase, int *pcount)
{
	int test, count = 0, check = QUERY_OK;
	my1list rdata;
	sqlite3_stmt *pstate;
	char pquery[MAX_QUERY_SIZE];
	list_setup(&rdata,LIST_TYPE_DEFAULT);
	/* list all raw data */
	strcpy(pquery,"SELECT UniqId, UnodId FROM my1dataraw;");
	if (sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
	{
		while (sqlite3_step(pstate)==SQLITE_ROW)
		{
			rawdata_t *pdata = (rawdata_t*) malloc(sizeof(rawdata_t));
			pdata->rawdid = sqlite3_column_int(pstate,0);
			pdata->nodeid = sqlite3_column_int(pstate,1);
			list_push_item(&rdata,(void*)pdata);
		}
	}
	else check = QUERY_ERROR_PREPARE;
	sqlite3_finalize(pstate);
	if (check!=QUERY_OK) return check;
	/* iterate through raw data list, remove orphan data */
	rdata.curr = 0x0;
	while (list_iterate(&rdata))
	{
		rawdata_t *pdata = (rawdata_t*) rdata.curr->item;
		sprintf(pquery,"SELECT NodeId FROM my1node WHERE UniqId=?;");
		if (sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
		{
			do {
				if (sqlite3_bind_int(pstate,1,pdata->nodeid)!=SQLITE_OK) {
					check=QUERY_ERROR_BINDVAL; break; }
				test = sqlite3_step(pstate);
				if (test!=SQLITE_ROW&&test!=SQLITE_DONE) {
					check=QUERY_ERROR_GOQUERY; break; }
			} while(0);
		}
		else check = QUERY_ERROR_PREPARE;
		sqlite3_finalize(pstate);
		if (check!=QUERY_OK) break;
		/** test results should still be valid */
		if (test==SQLITE_ROW)
		{
			strcpy(pquery,"DELETE FROM my1dataraw WHERE UniqId=?;");
			if (sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
			{
				do {
					if (sqlite3_bind_int(pstate,1,pdata->rawdid)!=SQLITE_OK) {
						check=QUERY_ERROR_BINDVAL; break; }
					test = sqlite3_step(pstate);
					if (test!=SQLITE_ROW&&test!=SQLITE_DONE) {
						check=QUERY_ERROR_GOQUERY; break; }
				} while(0);
			}
			else check = QUERY_ERROR_PREPARE;
			sqlite3_finalize(pstate);
			if (check!=QUERY_OK) break;
			count++; if (count%WORKBEAT==0) { putchar('.'); fflush(stdout); }
		}
	}
	list_clean(&rdata,&list_free_item);
	if (pcount) *pcount = count;
	return check;
}
/*----------------------------------------------------------------------------*/
int remove_node(sqlite3* pdbase, int uniqid)
{
	int test, count = 0, check = QUERY_OK;
	sqlite3_stmt *pstate;
	do
	{
		/* check if node is there! */
		char pquery[]="SELECT * FROM my1node WHERE UniqId==?1";
		if (sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
			check=QUERY_ERROR_PREPARE; break; }
		if (sqlite3_bind_int(pstate,1,uniqid)!=SQLITE_OK) {
			check=QUERY_ERROR_BINDVAL; break; }
		test = sqlite3_step(pstate);
		if (test!=SQLITE_ROW&&test!=SQLITE_DONE) {
			check=QUERY_ERROR_GOQUERY; break; }
	}
	while(0);
	sqlite3_finalize(pstate);
	if (check!=QUERY_OK) return check;
	if (test!=SQLITE_ROW) return count;
	do
	{
		/* remove data! */
		char pquery[]="DELETE FROM my1node WHERE UniqId==?1";
		if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)!=SQLITE_OK) {
			break; }
		if(sqlite3_bind_int(pstate,1,uniqid)!=SQLITE_OK) {
			check=QUERY_ERROR_BINDVAL; break; }
		if(sqlite3_step(pstate)!=SQLITE_DONE) {
			check=QUERY_ERROR_GOQUERY; break; }
		count++;
	}
	while(0);
	sqlite3_finalize(pstate);
	if (check!=QUERY_OK) return check;
	return count;
}
/*----------------------------------------------------------------------------*/
int compare_nodeid(void* pitem, void* qitem)
{
	wsnode *pnode = (wsnode*) pitem;
	wsnode *qnode = (wsnode*) qitem;
	return pnode->sensid == qnode->sensid ?
		pnode->nodeid - qnode->nodeid : pnode->sensid - qnode->sensid;
}
/*----------------------------------------------------------------------------*/
#define FILENAME_SIZE 128
/*----------------------------------------------------------------------------*/
int get_total_days(int year, int month)
{
	int totaldays[] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
	int days = totaldays[month], is_leap = 0;
	if(year%400==0) is_leap = 1;
	else if(year%100==0) is_leap = 0;
	else if(year%4==0) is_leap = 1;
	if(is_leap&&month==1)
		days++;
	return days;
}
/*----------------------------------------------------------------------------*/
int get_week_day(int year, int month, int day)
{
	/* using zeller's rule (Gregorian Calendar valid from 14/09/1752) */
	int yc, yy;
	/* adjust month */
	if(month<3) { month += 10; year--; }
	else { month -= 2; }
	/* represent year as century & year */
	yc = year / 100;
	yy = year % 100;
	return (day + ((13*month-1)/5) + yy + (yy/4) + (yc/4) - (yc*2)) % 7;
}
/*----------------------------------------------------------------------------*/
int get_month_week(int year, int month, int day)
{
	int test, temp, init, last;
	int days = get_total_days(year,month);
	int wday = get_week_day(year,month,day);
	/** get first monday & last sunday */
	test = (day%7);
	if(!test) test = 7;
	temp = (wday-1);
	if(temp<0) temp += 7;
	if(test<=temp) test += 7;
	init = test - temp;
	last = init + 20; /* 3 weeks later, -1 for sunday */
	if(days-last>7) last += 7;
	/* week count */
	if(day<init) test = 0;
	else test = ((day-init)/7)+1;
	/** check if need to compress first week */
	if(init<5)
	{
		if(test>0) test--;
	}
	/** check if need to compress final week */
	if((days-last)<4)
	{
		if(day>last) test--;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int create_datename(char* name, int year, int month, int day)
{
	int week = get_month_week(year,month,day);
	/* create string version for time */
	sprintf(name,"%s-%04d%02dW%d",name,year,month,week);
	/* append extension */
	sprintf(name,"%s.sqlite",name);
	return week;
}/*----------------------------------------------------------------------------*/
void create_filename(wsnode *pnode, char *pname)
{
	int loop, year, month, day;
	char buffer[8];
	for(loop=0;loop<4;loop++)
		buffer[loop] = pnode->timestring[loop];
	buffer[loop] = 0x0;
	year = atoi(buffer);
	for(loop=0;loop<2;loop++)
		buffer[loop] = pnode->timestring[loop+5];
	buffer[loop] = 0x0;
	month = atoi(buffer);
	for(loop=0;loop<2;loop++)
		buffer[loop] = pnode->timestring[loop+8];
	buffer[loop] = 0x0;
	day = atoi(buffer);
	strcpy(pname,SORTDATA_NAME);
	create_datename(pname,year,month,day);
}
/*----------------------------------------------------------------------------*/
#define FILTER_CANNOT_CREATE -1
#define FILTER_CANNOT_INSERT -2
#define FILTER_CANNOT_SETUP -3
#define FILTER_CANNOT_NCOPY -4
/*----------------------------------------------------------------------------*/
int create_node_base(my1list* plist, sqlite3* pdbase)
{
	int result = QUERY_OK;
	plist->curr = 0x0;
	while(list_iterate(plist))
	{
		sqlite3_stmt *pstate;
		wsnode *pnode = (wsnode*) plist->curr->item;
		char pquery[] = "INSERT INTO my1node (NodeId,SensId,AppsId) "
			"VALUES (?1,?2,?3);";
		if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
		{
			do
			{
				if(sqlite3_bind_int(pstate,1,pnode->nodeid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,2,pnode->sensid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_bind_int(pstate,3,pnode->appsid)!=SQLITE_OK) {
					result = QUERY_ERROR_BINDVAL; break; }
				if(sqlite3_step(pstate)!=SQLITE_DONE)
					result = QUERY_ERROR_EXECUTE;
			}
			while(0);
		}
		else result = QUERY_ERROR_PREPARE;
		sqlite3_finalize(pstate);
		if(result!=QUERY_OK) break;
	}
	return result;
}
/*----------------------------------------------------------------------------*/
int filter_savedata(wsnode *pnode, char *pname, my1list* pnodelist)
{
	int dbflags = SQLITE_OPEN_READWRITE | SQLITE_OPEN_PRIVATECACHE;
	sqlite3 *pdbase = 0x0;
	/* try to open only (no create) */
	if (sqlite3_open_v2(pname, &pdbase,dbflags,0x0)!=SQLITE_OK)
	{
		dbflags |= SQLITE_OPEN_CREATE;
		/* create AND insert nodes */
		if (sqlite3_open_v2(pname, &pdbase,dbflags,0x0)!=SQLITE_OK)
			return FILTER_CANNOT_CREATE;
		/* setup tables */
		if (setup_database(pdbase)<0)
			return FILTER_CANNOT_SETUP;
		/* insert nodes */
		if (create_node_base(pnodelist,pdbase)<0)
			return FILTER_CANNOT_NCOPY;
	}
	if (check_database(pdbase,pnode)<0)
		return FILTER_CANNOT_INSERT;
	sqlite3_close(pdbase);
	return 0;
}
/*----------------------------------------------------------------------------*/
int filter_rawd_base(sqlite3* pdbase,my1list* pnodelist,int* pcount)
{
	int count = 0, error = 0, test;
	wsnode tnode;
	sqlite3_stmt *pstate;
	char filename[FILENAME_SIZE];
	char pquery[] = "SELECT TimeStr,RawData FROM my1dataraw ORDER BY TimeStr;";
	if(sqlite3_prepare_v2(pdbase,pquery,-1,&pstate,0)==SQLITE_OK)
	{
		/* iterate through raw data in target db */
		while(sqlite3_step(pstate)==SQLITE_ROW)
		{
			char *ptext; void *pblob;
			/* prepare tnode */
			setup_wsnode(&tnode);
			/* get rawdata */
			ptext = (char*) sqlite3_column_text(pstate,0);
			strcpy(tnode.timestring,ptext);
			pblob = (void*) sqlite3_column_blob(pstate,1);
			tnode.countp = sqlite3_column_bytes(pstate,1);
			memcpy((void*)tnode.packet,pblob,tnode.countp);
			/* pre-calculate data count */
			tnode.dcount = (tnode.countp-XMESH_DATA_OFFSET)/sizeof(xword);
			tnode.pdata = (xword*) &tnode.packet[XMESH_DATA_OFFSET];
			/* create filename */
			create_filename(&tnode,filename);
			/* try to insert into db */
			test = filter_savedata(&tnode,filename,pnodelist);
			if(test<0) { error++; }
			count++; if (count%WORKBEAT==0) { putchar('.'); fflush(stdout); }
			/* clean up */
			clean_wsnode(&tnode);
		}
	}
	sqlite3_finalize(pstate);
	if(pcount) *pcount = count;
	return error;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	int loop, test, node = -1, error = 0, count;
	int do_command = COMMAND_UNKNOWN, do_help = 0, do_sort = 0;
	my1list nodes, sensb, rawds;
	/** database stuff */
	int dbflags = SQLITE_OPEN_READWRITE | SQLITE_OPEN_PRIVATECACHE;
	char *pdbf = 0x0,*pdb2 = 0x0,*pdbt = 0x0;
	sqlite3 *pdbase = 0x0, *pdbtgt = 0x0;

	/* print tool info */
	printf("\n%s - %s (%s)\n",MY1APP_PROGNAME,MY1APP_PROGINFO,MY1APP_PROGVERS);
	printf("  => by azman@my1matrix.net\n\n");

	/* check program arguments */
	if(argc>1)
	{
		/* first argument must be a command */
		if(!strcmp(argv[1],"nodelist"))
			do_command = COMMAND_NODELIST;
		else if(!strcmp(argv[1],"senslist"))
			do_command = COMMAND_SENSLIST;
		else if(!strcmp(argv[1],"nodecopy"))
			do_command = COMMAND_NODECOPY;
		else if(!strcmp(argv[1],"fixrdata"))
			do_command = COMMAND_FIXRDATA;
		else if(!strcmp(argv[1],"loadsens"))
			do_command = COMMAND_LOADSENS;
		else if(!strcmp(argv[1],"calcdata"))
			do_command = COMMAND_CALCDATA;
		else if(!strcmp(argv[1],"chkmerge"))
			do_command = COMMAND_CHKMERGE;
		else if(!strcmp(argv[1],"purgenod"))
			do_command = COMMAND_PURGENOD;
		else if(!strcmp(argv[1],"purgedat"))
			do_command = COMMAND_PURGEDAT;
		else if(!strcmp(argv[1],"keepdata"))
			do_command = COMMAND_KEEPDATA;
		else
			printf("Unknown command '%s'!\n",argv[1]);
		/* check remaining arguments even if no command */
		for(loop=2;loop<argc;loop++)
		{
			if(argv[loop][0]=='-') /* options! */
			{
				if(!strcmp(argv[loop],"--file"))
				{
					loop++;
					if(loop<argc) pdbf = argv[loop];
					else { printf("Cannot get DB filename!\n"); error++; }
				}
				else if(!strcmp(argv[loop],"--dest"))
				{
					loop++;
					if(loop<argc) pdb2 = argv[loop];
					else { printf("Cannot get DB filename (2)!\n"); error++; }
				}
				else if(!strcmp(argv[loop],"--sens"))
				{
					loop++;
					if(loop<argc) pdbt = argv[loop];
					else { printf("Cannot get text DB filename!\n"); error++; }
				}
				else if(!strcmp(argv[loop],"--node"))
				{
					loop++;
					if(loop<argc) node = atoi(argv[loop]);
					else { printf("Cannot get UniqId!\n"); error++; }
				}
				else if(!strcmp(argv[loop],"--sort"))
				{
					do_sort = 1;
				}
				else if(!strcmp(argv[loop],"--help"))
				{
					do_help = 1;
				}
				else
				{
					printf("Unknown option '%s'!\n",argv[loop]);
				}
			}
			else /* not an option? */
			{
				printf("Unknown parameter %s!\n",argv[loop]);
			}
		}
	}
	else
	{
		about();
	}

	/* cannot continue */
	if(error||!do_command) return ERROR_PARAM;
	if(!pdbf)
	{
		printf("Specify DB filename using --file option!\n");
		return ERROR_PARAM;
	}
	if((do_command==COMMAND_NODECOPY||do_command==COMMAND_CHKMERGE)&&!pdb2)
	{
		printf("Specify target DB filename using --dest option!\n");
		return ERROR_PARAM;
	}
	if(do_command==COMMAND_PURGENOD&&node<0)
	{
		printf("Specify valid nodeid using --node option!\n");
		return ERROR_PARAM;
	}

	/* check if user requested help */
	if(do_help)
	{
		about();
		return 0;
	}

	/* try to open database file */
	if(sqlite3_open_v2(pdbf, &pdbase,dbflags,0x0)!=SQLITE_OK)
	{
		printf("Cannot open database file '%s'!\n\n",pdbf);
		error = ERROR_DBOPEN;
	}
	else
	{
		printf("Database file '%s' opened!\n\n",pdbf);
		switch(do_command)
		{
			case COMMAND_NODELIST:
			{
				setup_node_list(&nodes,pdbase);
				printf("Node(s) in DB: %d.\n",nodes.count);
				if (do_sort) list_sort(&nodes,&compare_nodeid);
				list_browser(&nodes,&print_node,0x0);
				clean_node_list(&nodes);
				setup_rawd_list(&rawds,pdbase);
				printf("Raw data in DB: %d.\n",rawds.count);
				clean_rawd_list(&rawds);
				break;
			}
			case COMMAND_SENSLIST:
			{
				setup_sens_list(&sensb,pdbase);
				printf("Sens(s) in DB: %d.\n",sensb.count);
				list_browser(&sensb,&print_sens,0x0);
				clean_sens_list(&sensb);
				break;
			}
			case COMMAND_NODECOPY:
			{
				setup_node_list(&nodes,pdbase);
				printf("Node(s) in DB: %d.\n",nodes.count);
				if (do_sort) list_sort(&nodes,&compare_nodeid);
				/* try to open target database file */
				if(sqlite3_open_v2(pdb2, &pdbtgt,dbflags,0x0)==SQLITE_OK)
				{
					test = clone_node_list(&nodes,pdbtgt);
					printf("Copy result: %d.\n",test);
				}
				else
				{
					printf("Cannot open target DB file '%s'!\n",pdb2);
					printf("Command NOT executed!\n\n");
				}
				sqlite3_close(pdbtgt);
				clean_node_list(&nodes);
				break;
			}
			case COMMAND_FIXRDATA:
			{
				setup_node_list(&nodes,pdbase);
				printf("Node(s) in DB: %d.\n",nodes.count);
				setup_rawd_list(&rawds,pdbase);
				printf("Raw data in DB: %d.\n",rawds.count);
				printf("Processing raw data... ");
				fflush(stdout);
				test = reset_rawd_list(&rawds,pdbase,&nodes);
				printf("done!\n");
				printf("Raw data fixed: %d.\n",test);
				clean_rawd_list(&rawds);
				clean_node_list(&nodes);
				break;
			}
			case COMMAND_LOADSENS:
			{
				test = fopen_sens_list(&sensb,pdbt);
				if(test>BOARD_OK)
				{
					cp2db_sens_list(&sensb,pdbase);
					clean_sens_list(&sensb);
					setup_sens_list(&sensb,pdbase);
					printf("Sens(s) in DB: %d.\n",sensb.count);
					list_browser(&sensb,&print_sens,0x0);
				}
				else printf("Error loading from text DB! (%d)\n",test);
				clean_sens_list(&sensb);
				break;
			}
			case COMMAND_CALCDATA:
			{
				if(clear_data(pdbase))
				{
					setup_node_list(&nodes,pdbase);
					setup_sens_list(&sensb,pdbase);
					board_node_list(&nodes,&sensb);
					setup_rawd_list(&rawds,pdbase);
					printf("Raw data in DB: %d.\n",rawds.count);
					printf("Processing raw data... ");
					fflush(stdout);
					recal_rawd_list(&rawds,pdbase,&nodes);
					clean_rawd_list(&rawds);
					clean_node_list(&nodes);
					clean_sens_list(&sensb);
					printf("done!\n");
				}
				else printf("Error clearing data from DB!\n");
				break;
			}
			case COMMAND_CHKMERGE:
			{
				/* try to open target database file */
				if(sqlite3_open_v2(pdb2, &pdbtgt,dbflags,0x0)!=SQLITE_OK)
				{
					printf("Cannot open target DB file '%s'!\n",pdb2);
					printf("Command NOT executed!\n\n");
					break;
				}
				printf("Merging raw data... ");
				fflush(stdout);
				/* try to merge into original db */
				test = merge_rawd_base(pdbase,pdbtgt,&count);
				printf("done!\n");
				printf("Merge result: %d (%d)\n",test,count);
				/* clean up! */
				sqlite3_close(pdbtgt);
				break;
			}
			case COMMAND_PURGENOD:
			{
				setup_node_list(&nodes,pdbase);
				printf("Node(s) in DB (BEFORE): %d.\n",nodes.count);
				clean_node_list(&nodes);
				printf("Node %d being purge... ",node);
				count = remove_node(pdbase,node);
				printf("done. (%d)\n",count);
				setup_node_list(&nodes,pdbase);
				printf("Node(s) in DB (AFTER): %d.\n",nodes.count);
				clean_node_list(&nodes);
				break;
			}
			case COMMAND_PURGEDAT:
			{
				printf("Processing raw data... ");
				fflush(stdout);
				test = purge_rawd_base(pdbase,&count);
				printf("done!\n");
				printf("Purge result: %d (%d)\n",test,count);
				break;
			}
			case COMMAND_KEEPDATA:
			{
				setup_node_list(&nodes,pdbase);
				if (do_sort) list_sort(&nodes,&compare_nodeid);
				printf("Processing raw data... ");
				fflush(stdout);
				test = filter_rawd_base(pdbase,&nodes,&count);
				printf("done!\n");
				printf("Result: %d (%d)\n",test,count);
				break;
			}
		}
	}

	sqlite3_close(pdbase);
	if(!error) printf("\nDatabase file '%s' closed!\n\n",pdbf);

	return error;
}
/*----------------------------------------------------------------------------*/
