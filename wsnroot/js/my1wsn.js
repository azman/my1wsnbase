//
// my1 application data
//

function my1TinyH() {
	this.mDestAddr = null;
	this.mMsgType = null;
	this.mMsgGroup = null;
	this.mLength = null;
}

function my1MeshH() {
	this.mSrcAddr = null;
	this.mOrgAddr = null;
	this.mSeqNum = null;
	this.mAppID = null;
}

function my1SensH() {
	this.mBoardID = null;
	this.mPacketID = null;
	this.mParent = null;
}

function my1DataT() {
	this.mColor = 'rgb(255,255,255)';
	this.mRawData = new TimeSeries();
}

my1DataT.COLORS = { COLOR0:'rgb(255,0,0)', COLOR1:'rgb(0,255,0)',
	COLOR2:'rgb(0,0,255)', COLOR3:'rgb(255,255,0)', COLOR4:'rgb(255,0,255)',
	COLOR5:'rgb(0,255,255)', COLOR6:'rgb(0,0,0)', COLOR7:'rgb(255,255,255)',
	COLOR8:'rgb(255,165,0)', COLOR9:'rgb(192,192,192)' };

function my1Node(aUniqID,aNodeID,aSensID,aAppsID) {
	// private properties
	var mUniqID = aUniqID;
	var mNodeID = aNodeID;
	var mSensID = aSensID;
	var mAppsID = aAppsID;
	// read-only property (set on creation only!)
	this.getUniqID = function () {
		return mUniqID;
	}
	this.getNodeID = function () {
		return mNodeID;
	}
	this.getSensID = function () {
		return mSensID;
	}
	this.getAppsID = function () {
		return mAppsID;
	}
	// public properties - name information
	this.mFullName = "Node_"+convertHEX(mNodeID,4)+
		'_'+convertHEX(mSensID,2)+'_'+convertHEX(mAppsID,2);
	this.mName = this.mFullName;
	this.mCookName = readCookie(this.mFullName);
	if (this.mCookName!=null) {
		this.mName = this.mCookName;
	}
	this.mDataCount = -1;
	this.pTiny = new my1TinyH;
	this.pMesh = new my1MeshH;
	this.pSens = new my1SensH;
	this.mChart = null;
	this.mData = [];
	this.mInfo = null;
	this.mAQValid = false;
	this.mAQIndex = 0;
	this.chkValue = function(aData) {
		// get time stamp
		//var cTime = Date.prototype.getFromString(aData.timestamp);
		var cGoCreate = false;
		if(this.config.chkShowRawD==this.config.chkVoltView) {
			cGoCreate = true;
		}
		// check new chart if requested/required
		if(cGoCreate||this.mData.length!=aData.countD) {
			var maxvalue = 1023;
			this.config.chkVoltView = false;
			if(this.config.chkShowRawD==false&&aData.voltdata==1) {
				maxvalue = 0.0;
				for (var cCol = 0; cCol < aData.countD; cCol++) {
					if(maxvalue<aData["data"+cCol].vref) {
						maxvalue = aData["data"+cCol].vref;
					}
				}
				this.config.chkVoltView = true;
			}
			// create new chart add to time series
			this.mChart = null; // reset chart - do i need this?
			this.mChart = new SmoothieChart({
				grid: {strokeStyle:'rgb(125,0,0)',fillStyle:'rgb(60, 0, 0)',
					lineWidth: 1, millisPerLine:10000,verticalSections: 8,},
				maxValue:maxvalue, minValue:0,
				millisPerPixel:1000,
				labels: { fillStyle:'rgb(255,255,255)' }
				});
			this.mData = []; // reset data
			for (var cCol = 0; cCol < aData.countD; cCol++) {
				var cData = new my1DataT();
				cData.mColor = my1DataT.COLORS["COLOR"+cCol];
				this.mChart.addTimeSeries(cData.mRawData,{strokeStyle:cData.mColor});
				this.mData[this.mData.length] = cData;
			}
			// make sure we stream new data
			this.config.chkStreamed = false;
		}
		// add to time series
		for (var cCol = 0; cCol < aData.countD; cCol++) {
			var newdata;
			if(this.config.chkVoltView) {
				newdata = aData["data"+cCol].volt;
			} else {
				newdata = aData["data"+cCol].raw;
 			}
			//this.mData[cCol].mRawData.append(cTime.getTime(),newdata);
			this.mData[cCol].mRawData.append(new Date().getTime(),newdata);
		}
	}
	this.config = {
		chkIsActive: false,
		chkStreamed: false,
		chkShowRawD: false,
		chkVoltView: false,
		chkRegistered: false
	}
}

function my1WSNBase() {
	this.mNodes = [];
	this.mCount = 0; // active count!
	this.config = {
		chkShowAllN: false,
		chkDummy: false
	}
}

my1WSNBase.prototype.GetUniq = function (aUniqID) {
	var cNode = null;
	for (var loop=0;loop<this.mNodes.length;loop++) {
		if (this.mNodes[loop].getUniqID()==aUniqID) {
			cNode = this.mNodes[loop];
			break;
		}
	}
	return cNode;
}

my1WSNBase.prototype.GetNode = function (aNodeID,aSensID,aAppsID) {
	var cNode = null;
	for (var loop=0;loop<this.mNodes.length;loop++) {
		if (this.mNodes[loop].getNodeID()==aNodeID&&
			this.mNodes[loop].getSensID()==aSensID&&
			this.mNodes[loop].getAppsID()==aAppsID) {
			cNode = this.mNodes[loop];
			break;
		}
	}
	return cNode;
}

my1WSNBase.prototype.RefreshNodeList = function (aData) {
	// make all node inactive
	for (var loop=0;loop<this.mNodes.length;loop++) {
		this.mNodes[loop].config.chkIsActive = false;
	}
	this.mCount = 0;
	// activate nodes in list
	for (var t = 0; t < aData.countN; t++) {
		var cUniqID = aData["node"+t].uniqid;
		var cNodeID = aData["node"+t].nodeid;
		var cSensID = aData["node"+t].sensid;
		var cAppsID = aData["node"+t].appsid;
		var cNode = this.GetUniq(cUniqID);
		if(!cNode) {
			cNode = new my1Node(cUniqID,cNodeID,cSensID,cAppsID);
			cNode.mDataCount = aData["node"+t].dcount;
			this.mNodes[this.mNodes.length] = cNode;
		}
		// make active
		cNode.config.chkIsActive = true;
		this.mCount++;
	}
}

my1WSNBase.prototype.InsertNode = function (aData) {
	var cUniqID = aData.uniqid;
	var cNodeID = aData.nodeid;
	var cSensID = aData.sensid;
	var cAppsID = aData.appsid;
	var cNode = this.GetUniq(cUniqID);
	if(!cNode) {
		cNode = new my1Node(cUniqID,cNodeID,cSensID,cAppsID);
		cNode.mDataCount = aData.dcount;
		this.mNodes[this.mNodes.length] = cNode;
	}
	// make active
	cNode.config.chkIsActive = true;
	this.mCount++;
	return cNode;
}

my1WSNBase.prototype.RemoveNode = function (aUniqID) {
	// to REALLY remove it from array...
	//for (var loop=0;loop<this.mNodes.length;loop++) {
	//	if (this.mNodes[loop].getUniqID()==aUniqID) {
	//		this.mNodes.splice(loop,1);
	//		break;
	//	}
	//}
	var cNode = this.GetUniq(aUniqID);
	if(!cNode) return null;
	// make inactive
	cNode.config.chkIsActive = false;
	this.mCount--;
	return cNode;
}

my1WSNBase.prototype.UpdateNode = function (aData) {
	var cNode = this.GetUniq(aData.uniqid);
	var dNode = this.GetNode(aData.mesh.origin,
		aData.sens.board_id,aData.mesh.app_id);
	if(!cNode||cNode!=dNode) return null;
	if(!cNode.config.chkRegistered) return null;
	cNode.chkValue(aData);
	// make active
	cNode.config.chkIsActive = true;
	this.mCount++;
	return cNode;
}

//
// Extension for Date
//
// source http://www.tiddlywiki.com

Date.prototype.displayString = function () {
	var dateString = "";
	var tmpString = this.getFullYear() + "";
	while(tmpString.length<4)
		tmpString = '0' + tmpString;
	dateString = tmpString + dateString;
	tmpString = (this.getMonth()+1) + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString = tmpString + "/"+ dateString;
	tmpString = this.getDate() + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString = tmpString + "/"+ dateString;
	tmpString = this.getHours() + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString = dateString + " @ "+ tmpString;
	tmpString = this.getMinutes() + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString += tmpString;
	tmpString = this.getSeconds() + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString += tmpString;
	return dateString;
}

Date.prototype.convertToString = function () {
	var dateString = "";
	var tmpString = this.getFullYear() + "";
	while(tmpString.length<4)
		tmpString = '0' + tmpString;
	dateString += tmpString;
	tmpString = (this.getMonth()+1) + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString += tmpString;
	tmpString = this.getDate() + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString += tmpString;
	dateString += ':';
	tmpString = this.getHours() + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString += tmpString;
	tmpString = this.getMinutes() + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString += tmpString;
	tmpString = this.getSeconds() + "";
	while(tmpString.length<2)
		tmpString = '0' + tmpString;
	dateString += tmpString;
	return dateString;
};

function my1Date() {
	this.mYear = null;
	this.mMonth = null;
	this.mDay = null;
	this.mHour = null;
	this.mMinute = null;
	this.mSecond = null;
}

my1Date.prototype.getFromString = function (dateString) {
	dateString = dateString ? dateString.replace(/[^0-9]/g, "") : "";
	while(dateString.length<14)
		dateString += '0';
	this.mYear = parseInt(dateString.substr(0,4),10);
	this.mMonth = parseInt(dateString.substr(4,2),10);
	this.mDay = parseInt(dateString.substr(6,2),10);
	this.mHour = parseInt(dateString.substr(8,2)||"00",10);
	this.mMinute = parseInt(dateString.substr(10,2)||"00",10);
	this.mSecond = parseInt(dateString.substr(12,2)||"00",10);
}

Date.prototype.getFromString = function (dateString) {
	var cTemp = new my1Date();
	cTemp.getFromString(dateString);
	var cDate = new Date(cTemp.mYear, cTemp.mMonth-1, // it's javascript thingy
		cTemp.mDay, cTemp.mHour, cTemp.mMinute, cTemp.mSecond);
	return cDate;
};

//
// Cookies Load/Save
//
// source: http://www.quirksmode.org/js/cookies.html

function createCookie(name,value,days) {
	name = PRECOOKIE+'_'+name;
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	name = PRECOOKIE+'_'+name;
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}
