# makefile for my1wsnbase - base mote interface

PROJECT = my1wsnbase
CONSPRO = $(PROJECT)
CONSBIN = $(CONSPRO)
CONSOBJ = my1cons.o my1comlib.o my1xmesh.o my1list.o my1expr.o my1dbase.o
CONSOBJ += mongoose.o  my1wsock.o $(CONSBIN).o
TOOLPRO = my1wsntool
TOOLBIN = $(TOOLPRO)
TOOLOBJ = my1cons.o my1list.o my1expr.o my1dbase.o $(TOOLBIN).o
VIEWPRO = my1wsnview
VIEWBIN = $(VIEWPRO)
VIEWOBJ = my1cons.o my1comlib.o my1xmesh.o $(VIEWBIN).o
DOSWEEP = *.sqlite *.log
GARBAGE =

EXTPATH = ../my1termu/src
EX1PATH = ../my1codelib/src
EX2PATH = ../my1goose

PACKLBL = $(PROJECT)-$(shell cat VERSION)
PACKDIR = $(PACKLBL)-$(shell date +%Y%m%d)
PACKDAT = wsnroot sens/*.txt
PLATBIN ?= $(shell uname -m)

DELETE = rm -rf
COPY = cp -R
ARCHIVE = tar cjf
ARCHEXT = .tar.bz2
CONVERT = convert

CFLAGS += -Wall -pthread -I$(EXTPATH) -I$(EX1PATH) -I$(EX2PATH)
LFLAGS += -lm -ldl -lsqlite3
OFLAGS +=
LOCAL_FLAGS =
PACKDAT += $(CONSPRO) $(TOOLPRO)

VFLAGS = -DMY1APP_PROGVERS=\"$(shell date +%Y%m%d)\"
NFLAGS = -DMY1APP_PROGNAME=\"$(CONSBIN)\"
CC = $(CROSS_COMPILE)gcc
CPP = $(CROSS_COMPILE)g++
ifeq ($(DO_DEBUG),YES)
	LOCAL_FLAGS += -DMY1DEBUG -g
endif

debug: LOCAL_FLAGS += -DMY1DEBUG -g
pack: ARCNAME = $(PACKDIR)-$(PLATBIN)-$(shell date +%Y%m%d)$(ARCHEXT)
release: PACKDIR = $(PACKLBL)
release: VFLAGS = -DMY1APP_PROGVERS=\"$(shell cat VERSION)\"
test: CFLAGS = -DTEST_MODULE -Wall -pthread
test: LFLAGS = -lm
tool: NFLAGS = -DMY1APP_PROGNAME=\"$(TOOLBIN)\"
view: NFLAGS = -DMY1APP_PROGNAME=\"$(VIEWBIN)\"

cons: $(CONSPRO)

all: cons tool view

tool: $(TOOLPRO)

view: $(VIEWPRO)

new: clean all

debug: new

pack: new
	mkdir -pv $(PACKDIR)
	$(COPY) $(PACKDAT) $(PACKDIR)/
	$(CONVERT) $(PACKDIR)/wsnroot/favicon.xpm $(PACKDIR)/wsnroot/favicon.ico
	$(DELETE) $(PACKDIR)/wsnroot/favicon.xpm
	$(DELETE) $(ARCNAME)
	$(ARCHIVE) $(ARCNAME) $(PACKDIR)

release: pack

$(CONSPRO): $(CONSOBJ)
	$(CC) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS)

$(TOOLPRO): $(TOOLOBJ)
	$(CC) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS)

$(VIEWPRO): $(VIEWOBJ)
	$(CC) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS)

%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) $(LOCAL_FLAGS) $(NFLAGS) $(VFLAGS) -c $<

%.o: src/%.c
	$(CC) $(CFLAGS) $(LOCAL_FLAGS) $(NFLAGS) $(VFLAGS) -c $<

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAGS) -c $<

%.o: $(EXTPATH)/%.c
	$(CC) $(CFLAGS) -c $<

%.o: $(EX1PATH)/%.c $(EX1PATH)/%.h
	$(CC) $(CFLAGS) -c $<

%.o: $(EX1PATH)/%.c
	$(CC) $(CFLAGS) -c $<

%.o: $(EX2PATH)/%.c $(EX2PATH)/%.h
	$(CC) $(CFLAGS) -DUSE_WEBSOCKET -c $<

%.o: $(EX2PATH)/%.c
	$(CC) $(CFLAGS) -DUSE_WEBSOCKET -c $<

clean:
	-$(DELETE) $(CONSBIN) $(TOOLBIN) $(VIEWBIN) $(PACKLBL)* *.bz2 *.o

clear:
	-$(DELETE) $(GARBAGE) $(DOSWEEP)

sweep: clean clear
